function value = gaussian(x, mu, sigma) 
%GAUSSIAN evaluete value of x on normalized gaussian with defined parameters
%   It just calculate the gaussian value at x.

value  = (1/(sqrt(2*pi)*sigma))*exp(-((x-mu).^2)/(2*sigma^2));

% equivalent of 
%value  = normpdf(x, mu, sigma);

end
