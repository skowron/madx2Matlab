function [x, y] = ellipseCross(alfa, beta, emittance, nElements, mu1, mu2)
%[x, y] = ellipseCross(alfa, beta, emittance, nElements, mu1, mu2)
% It generates x,y coordinates of nElements equidistant points on the axes 
% of the ellipse defined by the Twiss parameters alfa, beta and emittance.
%
%
% mu1 and mu2 (default 0 and pi/2) are the angles at which to generate the
% cross legs in normalised phase space.
%
% davideg - Sept 2015

if nargin == 4
    mu1 = 0;
    mu2 = pi/2;
end


% normalised phase-space
% transformation
auxNormalisationMatrix = [...
    1/sqrt(beta), 0;...
    alfa/sqrt(beta), sqrt(beta)];

% compute also the inverse (actually I only use that)
auxNormalisationMatrixInverse = [...
    sqrt(beta), 0;...
    -alfa/sqrt(beta), 1/sqrt(beta)];

% generate my cross in normalised phase space, i.e. axes of circle with
% radious = sqrt(emittance)
auxRadius = sqrt(emittance);
auxRadius = linspace(-auxRadius, auxRadius, nElements);
auxX = [auxRadius.*cos(mu1), auxRadius.*cos(mu2)];
auxY = [auxRadius.*sin(mu1), auxRadius.*sin(mu2)];
%auxX = [linspace(-auxRadius, auxRadius, nElements), zeros(1, nElements)];
%auxY = [zeros(1, nElements), linspace(-auxRadius, auxRadius, nElements)];

aux = auxNormalisationMatrixInverse * [auxX; auxY];
x=aux(1,:);
y=aux(2,:);


% 
% auxGamma = (1+alfa^2)/beta;
% auxM = tan(atan(2*alfa/(auxGamma - beta))/2);
% 
% maxX1 = myMaxX(alfa, beta, auxGamma, emittance, auxM);
% maxX2 = myMaxX(alfa, beta, auxGamma, emittance, -1/auxM);
% 
% x1 = linspace(-maxX1,maxX1,nElements);
% x2 = linspace(-maxX2,maxX2,nElements);
% y1 = x1*auxM;
% y2 = x2*(-1/auxM);
% x = [x1(:); x2(:)];
% y = [y1(:); y2(:)];
% 
% end
% 
% 
% function X = myMaxX(alfa, beta, gamma, emittance, M)
% % it is the "stupid" function that finds the last x coordinate of the line
% % with coefficient m that belongs to the given Twiss ellipse...
%     X = sqrt(emittance/(gamma + 2*alfa*M + beta*M^2));
% end
