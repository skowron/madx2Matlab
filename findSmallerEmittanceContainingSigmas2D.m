function [emittance, center] = findSmallerEmittanceContainingSigmas2D( gaussianBeamCovariances, gaussianBeamCenters, inputBeta, inputAlfa, nSigmaEmit )
%[emittance, center] = findSmallerEmittanceContainingSigmas2D( sigmas, centers, inputBeta, inputAlfa, nSigmaEmit )
%   [tot_sigma, tot_centers] = combineSigmas( sigmas, centers, weights )
%
%   "gaussianBeamCovariances" is an array of n cells where in each is a covariance matrix of a
%   Gaussian beamlet. Be carefull that this are not a Twiss-like matrix, but
%   they has also to contains the emittance of the beam-lets, i.e.
%   \epsilon*TwissMatrix.
%   "gaussianBeamCenters" is a matrix of n rows (i.e. number of sigmas) and 2 columns
%   with the center of the above bi-Gaussian beamlets.
%   "inputBeta" and "inputAlfa" are the Twiss parameters of the final beam
%   that has to contain all the single beam-lets.
%   "nSigmaEmit" is the number of 
%
%   The output is the minimal emittance such that the ellipse 
%


%%%%%%%
% used for numerical parameters.
nPointsPerEllipse = 1000; %Increase this number if you want to be more precise.
%%%%%%%


if ~iscell(gaussianBeamCovariances)
    error('The first argument must be a cell array of sigma matrices.')
else % check that all the matrices are fine
    nSigmas = length(gaussianBeamCovariances);
    
    sizeSigma = size(gaussianBeamCovariances{1});
    nVariables = sizeSigma(1);
    if sizeSigma(1) ~= sizeSigma(2)
        error('The first sigma matrix looks not to be square... impossible to continue.')
    end
    
    for i=2:nSigmas
        if size(gaussianBeamCovariances{i}) ~= sizeSigma
            error(['The ',num2str(i),'-th sigma matrix looks to be different in size than the first one... impossible to continue.'])
        end
    end
end
% assign default input values if needed
if nargin == 1
    gaussianBeamCenters = zeros(nSigmas,nVariables);
    inputBeta = 1;
    inputAlfa = 0;
    nSigmaEmit = 1;
elseif nargin == 2
    inputBeta = 1;
    inputAlfa = 0;
    nSigmaEmit = 1;
elseif nargin == 3
    inputAlfa = 0;
    nSigmaEmit = 1;
elseif nargin == 4
    nSigmaEmit = 1;
end


%%%%%%%%%
% now the proper calculation

% first I transform all the covariances in the normalised phase-space
auxNormalisationMatrix = [...
    1/sqrt(inputBeta), 0;...
    inputAlfa/sqrt(inputBeta), sqrt(inputBeta)];

% compute also the inverse
auxNormalisationMatrixInverse = [...
    sqrt(inputBeta), 0;...
    -inputAlfa/sqrt(inputBeta), 1/sqrt(inputBeta)];

% go to normalised phase-space with the centers of the beamlets.
allCentersNormalised = (auxNormalisationMatrix * gaussianBeamCenters')';

% prepare the space for all the points.
pointsMatrix = nan(nSigmas*nPointsPerEllipse, 2);

% compute all the points in normalised phase space 
for i=1:nSigmas
    auxNewSigma = auxNormalisationMatrix * gaussianBeamCovariances{i} * auxNormalisationMatrix';
    auxEmit = sqrt(det(auxNewSigma));
    auxNewSigmasNormal = auxNewSigma/auxEmit;
    [auxx, auxy] = ellipsePoint(linspace(0,2*pi,nPointsPerEllipse), -auxNewSigmasNormal(1,2), auxNewSigmasNormal(1,1), auxEmit*(nSigmaEmit^2));
    
    % store points
    pointsMatrix((i-1)*nPointsPerEllipse + (1:nPointsPerEllipse),:) = ...
        [allCentersNormalised(i,1) + auxx(:), allCentersNormalised(i,2) + auxy(:)];
end

% compute the min radious
[C,R] = minboundcircle(pointsMatrix(:,1),pointsMatrix(:,2));
% try
%     [R,C,~]=ExactMinBoundCircle(pointsMatrix);
% catch
%     disp('asdf')
% end


% compute outputs, with center transformed back to normal space.
emittance = R^2;
center = auxNormalisationMatrixInverse*C(:);
end

