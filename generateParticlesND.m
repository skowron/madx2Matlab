function particles = generateParticlesND(nParticles, sigmaMatrix, centers)
% particles = generateParticlesND(nparticles, sigmaMatrix, centers)
% it generates n particles distribuited accordingly to the given
% sigmaMatrix and the centers array (default is zero)
%
% the output "particles" is a matrix of nVariables X nParticles elements
%
% davideg Sep 2014

if nargin == 2
    centers = zeros(size(sigmaMatrix,1),1);
end

% generate particles
particles = mvnrnd(centers, sigmaMatrix, nParticles)';

end
