function varargout = fwhm(x, y)
%[fullwidth, startx, endx] = fwhm(x, y)
% This function determines the full width at half maximum of the given function
% identified by y = f(x);
%
% It is a very stupid one, so it might not be good in all the cases!
%
% davideg

x = x(:);
y = y(:);
if length(x) ~= length(y)
    error('two vectors must be of the same length')
end

% wrong! - davide Jan 2016. Just the max of the given profile!
% [maxy, maxyidx] = max(y);
% miny = min(y);
% y=y-miny;
% maxy=maxy-miny;
maxy = max(y);

startIdx = find(y > (maxy/2),1,'first');

% very bad: davideg - jan 2016
%endIdx = maxyidx - 1 + find(y(maxyidx:end) < (maxy/2),1,'first');
endIdx = length(y) + 1 - find(y(end:-1:1) > (maxy/2),1,'first');


% linear interpolation between consecutive points.
if startIdx > 1 
    startX = x(startIdx-1) + (maxy/2 - y(startIdx-1))*(x(startIdx)-x(startIdx-1))/(y(startIdx)-y(startIdx-1));
else
    startX = x(startIdx);
end
%
if endIdx < length(x)
    endX = x(endIdx) + (y(endIdx) - maxy/2)*(x(endIdx+1) - x(endIdx))/(y(endIdx)-y(endIdx+1));
    %endX = (x(endIdx+1) + x(endIdx))/2;
else
    endX = x(endIdx);
end
%
Fullw = abs(endX - startX);


% % debug
% figure(10)
% plot(x, y,'.-')
% hold on
% plot(x(startIdx), y(startIdx),'ro')
% plot(x(startIdx-1), y(startIdx-1),'ro')
% plot(x(endIdx), y(endIdx),'ro')
% plot(x(endIdx+1), y(endIdx+1),'ro')
% plot([startX,endX],[maxy/2, maxy/2],'r.-') 
% hold off

% prepare output
switch nargout
    case 1
        varargout(1) = {Fullw};
    case 2
        varargout(1) = {Fullw};
        varargout(2) = {startX};
    case 3
        varargout(1) = {Fullw};
        varargout(2) = {startX};
        varargout(3) = {endX};
    otherwise
        % nothing. just say.
        fprintf('Full Width Half Max = %f\n', Fullw);
end

end

