%
% test script to test madx2matlab class
% 
% davide.gamba@cern.ch

% MODIFY HERE TO MAKE IT PROPERLY WORKING! i.e. set here the directory where actually it is this script
myPath='/user/ctf3op/davideg/work/madx2MatlabLib/demo';
addpath(fullfile(myPath,'../'));
cd(myPath);


% parse file
myFileName = 'demoLHCTwissTable.dat';
parsedTable = madx2matlab.parseTFSTableFromFile(myFileName);


% plot it
auxAx = [];
auxAx(1) = subplot(2,1,1);
plot([parsedTable.DATA.S], [parsedTable.DATA.BETX],'.-')


% try to add machine lattice plots
try
    auxAx(2) = subplot(2,1,2);
    myScaleY = 0.5;
    madx2matlab.plotMachineTwissElements(parsedTable, myScaleY);
    
    linkaxes(auxAx,'x')
catch e
    disp(['Impossible to plot machine lattice: ',e.message]);
end


