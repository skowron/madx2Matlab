function [tot_sigma, tot_centers] = combineSigmas( sigmas, centers, weights )
%COMBINESIGMAS combines sigma matrices
%   [tot_sigma, tot_centers] = combineSigmas( sigmas, centers, weights )
%
%   "sigmas" is the only mandatory argument and consists of an array of
%   n cells where in each is a sigma matrix.
%   "centers" is a matrix of n rows (i.e. number of sigmas) and m columns
%   (i.e. number of variables or size of the sigma matrix).
%   It must contains the mean values of all the variables (<x_i>) (default
%   are all zeros).
%   "weights" is a simple array of weights to be used for the combination.
%   (default are all ones).
%
%   In output is the total sigma matrix and the total center positions.
%

if ~iscell(sigmas)
    error('The first argument must be a cell array of sigma matrices.')
else % check that all the matrices are fine
    nSigmas = length(sigmas);
    
    sizeSigma = size(sigmas{1});
    nVariables = sizeSigma(1);
    if sizeSigma(1) ~= sizeSigma(2)
        error('The first sigma matrix looks not to be square... impossible to continue.')
    end
    
    for i=2:nSigmas
        if size(sigmas{i}) ~= sizeSigma
            error(['The ',num2str(i),'-th sigma matrix looks to be different in size than the first one... impossible to continue.'])
        end
    end
end
% assign default input values if needed
if nargin == 1
    centers = zeros(nSigmas,nVariables);
    weights = ones(nSigmas,1);
elseif nargin == 2
    if size(centers) ~= [nSigmas, nVariables]
        error('The matrix holding the center position of all the variables looks to be wrong in dimension. Please check!')
    end
    weights = ones(nSigmas,1);
end

%%%%%%%%%
% now the proper calculation

% normalize weights
sumWeights = sum(weights);
weights = (weights(:)./sumWeights)';

% compute total centers
tot_centers = (weights*centers)';

% compute total sigma
tot_sigma = zeros(nVariables);

for i=1:nVariables
    for j=1:nVariables
        for k=1:nSigmas
            tot_sigma(i,j) = tot_sigma(i,j) + ...
                weights(k)*(sigmas{k}(i,j)+centers(k,i)*centers(k,j));
        end
        tot_sigma(i,j) = tot_sigma(i,j) - tot_centers(i)*tot_centers(j);
    end
end


end

