#!/usr/bin/python           
#
# This script starts a server that listen from connection from 
# madxClient.py.
# It will be attached to a madx instance that is start with an initial
# madx starting script.
# The client is supposed to receive some commands from stdinput, 
# then when it receives EOF, it contacts the server, runs the commands,
# and finally return the madx output to stdout.
# In case the commands make the madx CLI crash, the server will automatically
# restart the CLI and re-run the initial madx script ONLY!
#
# April 2014 - davide.gamba@cern.ch

import socket               # Import socket module
import struct
import pexpect              # http://pexpect.readthedocs.org/en/latest/
import hashlib
import sys
import signal

#configuration variables
madxStartScriptFile = './serverScript.madx'
madxExecutable = '../madx'
madxCommandTimeOut = 60
serverPort = 5061
serverBindingHost = ''

###########################
# some function definition

#function to initialize madx model
def startMadx():
  "Starts the madx executable and execute initial madx script."
  print "Re-Starting MadX"

  try:  
    # first read the script file 
    with open(madxStartScriptFile) as file:
      madxScriptData = file.read()
    
    # compute md5 of initial script, and print it at the end of the script.
    auxmd5 = hashlib.md5(madxScriptData).hexdigest()
    madxScriptData = (madxScriptData + '\n' +
      'option,-echo;\n' +
      'print, text="' + auxmd5 + '";\n')
    
    #create madx process
    mymadx = pexpect.spawn (madxExecutable)
    # wait for the prompt
    mymadx.expect ('X: ==>.*')
    # send initial script data and expect 2 times the md5sum calculated 
    mymadx.sendline (madxScriptData)
    mymadx.expect_exact (auxmd5, madxCommandTimeOut)
    mymadx.expect_exact (auxmd5, madxCommandTimeOut)
  except pexpect.EOF as e:
    sys.exit("MADX somehow terminated. Impossible to continue.")
  except pexpect.TIMEOUT as e:
    sys.exit("Execution TIMEOUT: The main model script takes to much time to execute. You can try to increase TIMEOUT. Now it is impossible to continue.")
  except IOError as e:
    sys.exit("General IOError({0}) starting madx: {1}.\n Impossible to continue.".format(e.errno, e.strerror))

  # IF we are here without exception, everything is ready.
  return mymadx


# function to send data over a connection
def mySendData(the_socket, data):
    # from http://code.activestate.com/recipes/408859/
    the_socket.sendall(struct.pack('>i', len(data))+data)

# function to receive data over a connection
def myReceiveData(the_socket):
    # from http://code.activestate.com/recipes/408859/
    #data length is packed into 4 bytes
    total_len=0;total_data=[];size=sys.maxint
    size_data=sock_data='';recv_size=8192
    while total_len<size:
        sock_data=the_socket.recv(recv_size)
        if not total_data:
            if len(sock_data)>4:
                size_data+=sock_data
                size=struct.unpack('>i', size_data[:4])[0]
                recv_size=size
                if recv_size>524288:recv_size=524288
                total_data.append(size_data[4:])
            else:
                size_data+=sock_data
        else:
            total_data.append(sock_data)
        total_len=sum([len(i) for i in total_data ])
    return ''.join(total_data)




# function to execute script to my madx session
def computeMadxResponse(mymadx, script):
  output = ''

  # compute md5 of the new commands and add it to script
  auxmd5 = hashlib.md5(script).hexdigest()
  script = (script + '\n' +
    'option,-echo;\n' +
    'print, text="' + auxmd5 + '";\n')

  # debug
  #print "I will execute:"
  #print script

  # execute script
  try:
    # send initial script data and expect 2 times the md5sum calculated 
    mymadx.sendline(script)
    mymadx.expect_exact (auxmd5+'";', madxCommandTimeOut)
    mymadx.expect_exact (auxmd5, madxCommandTimeOut)
    output = mymadx.before
  except pexpect.EOF as e:
    print "MADX somehow terminated."
    output = -1
  except pexpect.TIMEOUT as e:
    print "Script execution timeout. Think of increasing server timeout."
    output = -1

  return output


# function to nicely close server
def signal_handler(signal, frame):
  print "The server is going to exit."
  mysocket.close
  mymadx.close(force=True)
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


###########################
#start of the real script

# initialize madxmodel
mymadx = startMadx()

# create socket connection
mysocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)# Create a socket object
mysocket.bind((serverBindingHost, serverPort))        # Bind to the port
mysocket.listen(5)                          # Now wait for client connection.

# start infinite loop to accept connections
while True:
   auxConnection, addr = mysocket.accept()  # Establish connection with client.
   print 'Got connection from', addr

   # receive all the data from client
   receivedScript = myReceiveData(auxConnection)

   # try to execute the script to madx
   madxOutput = computeMadxResponse(mymadx, receivedScript)

   # if output is -1, I need to restart the server.
   if madxOutput == -1:
     mymadx.close(force=True)
     mymadx = startMadx()
     madxOutput = str(madxOutput);
   else: # clean unwanted madx CLI prompt
     madxOutput = madxOutput.replace('X: ==>','')

   # send back data (or error) to client
   mySendData(auxConnection, madxOutput)

   # close the connection
   auxConnection.close()                # Close the connection

