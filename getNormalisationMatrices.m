function varargout = getNormalisationMatrices(inputBeta, inputAlfa)
%[normalisationMatrix, inverseMatrix] = getNormalisationMatrices(inputBeta, inputAlfa)
%
% this function returns the 2x2 transformation matrices to go into
% normalised phase space and the inverse matrix if nargout == 2.
%
% davideg - Nov 2016

% normalisation matrix
normalisationMatrix = [...
    1/sqrt(inputBeta), 0;...
    inputAlfa/sqrt(inputBeta), sqrt(inputBeta)];

% compute also the inverse
inverseMatrix = [...
    sqrt(inputBeta), 0;...
    -inputAlfa/sqrt(inputBeta), 1/sqrt(inputBeta)];

% prepare outputs
if nargout <= 1
    varargout(1) = {normalisationMatrix};
elseif nargout == 2
    varargout(1) = {normalisationMatrix};
    varargout(2) = {inverseMatrix};
end

end