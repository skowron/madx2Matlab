function [x, angle] = ellipsePoint(theta, alfa, beta, emittance)
% [x, angle] = ellipsePoint(theta, alfa, beta, emittance)
% it just produce [x, angle] coordinates of a point on a given ellipse at a
% specific theta... useful for printing an ellipse with given
% alfa/beta/emittance...
%
% davideg - July 2014
%

x = sqrt(emittance*beta)*cos(theta);
angle = -sqrt(emittance/beta)*(alfa*cos(theta)-sin(theta));

end
