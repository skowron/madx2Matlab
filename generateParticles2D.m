function [Xs, XAngles, Ys, YAngles] = generateParticles2D(nparticles, alphaX, betaX, stdgeoemittanceX, alphaY, betaY, stdgeoemittanceY)
% [Xs, XAngles, Ys, YAngles] = generateParticles2D(nparticles, alphaX, betaX, stdgeoemittanceX, alphaY, betaY, stdgeoemittanceY)
% it generates n particles distribuited accordingly to the given Twiss
% parameters with NO coupling between the two planes.
%
% davideg Sep 2014

% generate sigma matrix

myGammaX = (1+alphaX^2)/betaX;
mySigmaX = [betaX*stdgeoemittanceX -alphaX*stdgeoemittanceX;...
    -alphaX*stdgeoemittanceX myGammaX*stdgeoemittanceX];

myGammaY = (1+alphaY^2)/betaY;
mySigmaY = [betaY*stdgeoemittanceY -alphaY*stdgeoemittanceY;...
    -alphaY*stdgeoemittanceY myGammaY*stdgeoemittanceY];

mySigma = zeros(4);
mySigma(1:2,1:2) = mySigmaX;
mySigma(3:4,3:4) = mySigmaY;


% generate particles
auxMat = mvnrnd([0 0 0 0], mySigma, nparticles);
Xs = auxMat(:,1);
XAngles = auxMat(:,2);
Ys = auxMat(:,3);
YAngles = auxMat(:,4);

end
