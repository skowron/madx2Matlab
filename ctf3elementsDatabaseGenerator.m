function output = ctf3elementsDatabaseGenerator()

% to be filled: madxName, setSignal, getSignal; ...
data = {...
    'CL.IQFA0405','CL.QFA0405/SettingPPM#current','CL.QFA0405/Acquisition#current';...
    'CL.IQDA0415','CL.QDA0415/SettingPPM#current','CL.QDA0415/Acquisition#current';...
    'CL.IQFA0420','CL.QFA0420/SettingPPM#current','CL.QFA0420/Acquisition#current';...
    'CL.IQFA0460','CL.QFA0460/SettingPPM#current','CL.QFA0460/Acquisition#current';...
    'CL.IQDA0465','CL.QDA0465/SettingPPM#current','CL.QDA0465/Acquisition#current';...
    'CL.IQFA0505','CL.QFA0505/SettingPPM#current','CL.QFA0505/Acquisition#current';...
    'CL.IQDA0510','CL.QDA0510/SettingPPM#current','CL.QDA0510/Acquisition#current';...
    'CL.IQDA0605','CL.QDA0605-S/SettingPPM#current','CL.QDA0605-S/Acquisition#current';...
    'CL.IQFB0610','CL.QFB0610/SettingPPM#current','CL.QFB0610/Acquisition#current';...
    'CL.IQDB0705','CL.QDB0705-S/SettingPPM#current','CL.QDB0705-S/Acquisition#current';...
    'CL.IQFC0710','CL.QFC0710/SettingPPM#current','CL.QFC0710/Acquisition#current';...
    'CL.IQDB0805','CL.QDB0805/SettingPPM#current','CL.QDB0805/Acquisition#current';...
    'CL.IQFC0810','CL.QFC0810/SettingPPM#current','CL.QFC0810/Acquisition#current';...
    'CL.IQDB0815','CL.QDB0815/SettingPPM#current','CL.QDB0815/Acquisition#current';...
    'CL.IQDB0905','CL.QDB0905-S/SettingPPM#current','CL.QDB0905-S/Acquisition#current';...
    'CL.IQFC0910','CL.QFC0910/SettingPPM#current','CL.QFC0910/Acquisition#current';...
    'CL.IQDB1005','CL.QDB1005/SettingPPM#current','CL.QDB1005/Acquisition#current';...
    'CL.IQFC1010','CL.QFC1010/SettingPPM#current','CL.QFC1010/Acquisition#current';...
    'CL.IQDB1015','CL.QDB1015/SettingPPM#current','CL.QDB1015/Acquisition#current';...
    'CL.IQDB1105','CL.QDB1105-S/SettingPPM#current','CL.QDB1105-S/Acquisition#current';...
    'CL.IQFC1110','CL.QFC1110/SettingPPM#current','CL.QFC1110/Acquisition#current';...
    'CL.IQDC1205','CL.QDC1205-S/SettingPPM#current','CL.QDC1205-S/Acquisition#current';...
    'CL.IQFD1210','CL.QFD1210/SettingPPM#current','CL.QFD1210/Acquisition#current';...
    'CL.IQDD1305','CL.QDD1305-S/SettingPPM#current','CL.QDD1305-S/Acquisition#current';...
    'CL.IQFD1310','CL.QFD1310/SettingPPM#current','CL.QFD1310/Acquisition#current';...
    'CL.IQDD1405','CL.QDD1405-S/SettingPPM#current','CL.QDD1405-S/Acquisition#current';...
    'CL.IQFD1410','CL.QFD1410/SettingPPM#current','CL.QFD1410/Acquisition#current';...
    'CL.IQDD1505','CL.QDD1505-S/SettingPPM#current','CL.QDD1505-S/Acquisition#current';...
    'CL.IQFD1510','CL.QFD1510/SettingPPM#current','CL.QFD1510/Acquisition#current';...
    ...
    'CL.IBHA0425','CL.BHA0425-S/SettingPPM#current','CL.BHA0425-S/Acquisition#current';...
    'CL.IBHA0430','CL.BHA0430-S/SettingPPM#current','CL.BHA0430-S/Acquisition#current';...
    ...
    'CL.IDHC0480','CL.DHC0480/SettingPPM#current','CL.DHC0480/Acquisition#current';...
    'CL.IDHC0520','CL.DHC0520/SettingPPM#current','CL.DHC0520/Acquisition#current';...
    'CL.IDHC0620','CL.DHC0620/SettingPPM#current','CL.DHC0620/Acquisition#current';...
    'CL.IDHC0720','CL.DHC0720/SettingPPM#current','CL.DHC0720/Acquisition#current';...
    'CL.IDHC0820','CL.DHC0820/SettingPPM#current','CL.DHC0820/Acquisition#current';...
    'CL.IDHC0920','CL.DHC0920/SettingPPM#current','CL.DHC0920/Acquisition#current';...
    'CL.IDHC1020','CL.DHC1020/SettingPPM#current','CL.DHC1020/Acquisition#current';...
    'CL.IDHD1120','CL.DHD1120/SettingPPM#current','CL.DHD1120/Acquisition#current';...
    'CL.IDHD1220','CL.DHD1220/SettingPPM#current','CL.DHD1220/Acquisition#current';...
    'CL.IDHD1320','CL.DHD1320/SettingPPM#current','CL.DHD1320/Acquisition#current';...
    'CL.IDHD1420','CL.DHD1420/SettingPPM#current','CL.DHD1420/Acquisition#current';...
    'CL.IDHD1520','CL.DHD1520/SettingPPM#current','CL.DHD1520/Acquisition#current';...
    ...
    'CL.IDVC0480','CL.DVC0480/SettingPPM#current','CL.DVC0480/Acquisition#current';...
    'CL.IDVC0520','CL.DVC0520/SettingPPM#current','CL.DVC0520/Acquisition#current';...
    'CL.IDVC0620','CL.DVC0620/SettingPPM#current','CL.DVC0620/Acquisition#current';...
    'CL.IDVC0720','CL.DVC0720/SettingPPM#current','CL.DVC0720/Acquisition#current';...
    'CL.IDVC0820','CL.DVC0820/SettingPPM#current','CL.DVC0820/Acquisition#current';...
    'CL.IDVC0920','CL.DVC0920/SettingPPM#current','CL.DVC0920/Acquisition#current';...
    'CL.IDVC1020','CL.DVC1020/SettingPPM#current','CL.DVC1020/Acquisition#current';...
    'CL.IDVD1120','CL.DVD1120/SettingPPM#current','CL.DVD1120/Acquisition#current';...
    'CL.IDVD1220','CL.DVD1220/SettingPPM#current','CL.DVD1220/Acquisition#current';...
    'CL.IDVD1320','CL.DVD1320/SettingPPM#current','CL.DVD1320/Acquisition#current';...
    'CL.IDVD1420','CL.DVD1420/SettingPPM#current','CL.DVD1420/Acquisition#current';...
    'CL.IDVD1520','CL.DVD1520/SettingPPM#current','CL.DVD1520/Acquisition#current';...
    ...
    'CT.IQDD0110','CT.QDD0110/SettingPPM#current','CT.QDD0110/Acquisition#current';...
    'CT.IQFD0130','CT.QFD0130/SettingPPM#current','CT.QFD0130/Acquisition#current';...
    'CT.IQFD0150','CT.QFD0150/SettingPPM#current','CT.QFD0150/Acquisition#current';...
    'CT.IQDD0220','CT.QDD0220-S/SettingPPM#current','CT.QDD0220-S/Acquisition#current';...
    'CT.IQFE0230','CT.QFE0230-S/SettingPPM#current','CT.QFE0230-S/Acquisition#current';...
    'CT.IQDF0245','CT.QDF0245-S/SettingPPM#current','CT.QDF0245-S/Acquisition#current';...
    'CT.IQFE0250','CT.QFE0250/SettingPPM#current','CT.QFE0250/Acquisition#current';...
    'CT.IQFD0310','CT.QFD0310/SettingPPM#current','CT.QFD0310/Acquisition#current';...
    'CT.IQFD0330','CT.QFD0330/SettingPPM#current','CT.QFD0330/Acquisition#current';...
    'CT.IQDD0350','CT.QDD0350/SettingPPM#current','CT.QDD0350/Acquisition#current';...
    'CT.IQDF0410','CT.QDF0410/SettingPPM#current','CT.QDF0410/Acquisition#current';...
    'CT.IQFF0420','CT.QFF0420/SettingPPM#current','CT.QFF0420/Acquisition#current';...
    'CT.IQDF0470','CT.QDF0470/SettingPPM#current','CT.QDF0470/Acquisition#current';...
    'CT.IQFF0480','CT.QFF0480/SettingPPM#current','CT.QFF0480/Acquisition#current';...
    'CT.IQDD0520','CT.QDD0520/SettingPPM#current','CT.QDD0520/Acquisition#current';...
    'CT.IQFD0530','CT.QFD0530/SettingPPM#current','CT.QFD0530/Acquisition#current';...
    'CT.IQFH0610','CT.QFH0610/SettingPPM#current','CT.QFH0610/Acquisition#currentAverage';...
    'CT.IQFH0620','CT.QFH0620/SettingPPM#current','CT.QFH0620/Acquisition#currentAverage';...
    'CT.IQFH0640','CT.QFH0640/SettingPPM#current','CT.QFH0640/Acquisition#currentAverage';...
    'CT.IQDI0650','CT.QDI0650/SettingPPM#current','CT.QDI0650/Acquisition#currentAverage';...
    'CT.IQFF0660','CT.QFF0660/SettingPPM#current','CT.QFF0660/Acquisition#currentAverage';...
    'CT.IQFH0680','CT.QFH0680/SettingPPM#current','CT.QFH0680/Acquisition#currentAverage';...
    'CT.IQFF0690','CT.QFF0690/SettingPPM#current','CT.QFF0690/Acquisition#currentAverage';...
    'CT.IQDF0720','CT.QDF0720/SettingPPM#current','CT.QDF0720/Acquisition#currentAverage';...
    'CT.IQFJ0730','CT.QFJ0730/SettingPPM#current','CT.QFJ0730/Acquisition#currentAverage';...
    'CT.IQDJ0740','CT.QDJ0740/SettingPPM#current','CT.QDJ0740/Acquisition#currentAverage';...
    'CT.IQFG0750','CT.QFG0750/SettingPPM#current','CT.QFG0750/Acquisition#currentAverage';...
    ...
    'CT.IBHD0490','CT.BHD0490/SettingPPM#current','CT.BHD0490/Acquisition#current';...
    'CT.IBHD0510','CT.BHD0510/SettingPPM#current','CT.BHD0510/Acquisition#current';...
    'CT.IDHD0160','CT.DHD0160/SettingPPM#current','CT.DHD0160/Acquisition#current';...
    'CT.IDHD0308','CT.DHD0308/SettingPPM#current','CT.DHD0308/Acquisition#currentAverage';...
    'CT.IDHD0360','CT.DHD0360/SettingPPM#current','CT.DHD0360/Acquisition#current';...
    'CT.IDHF0365','CT.DHF0365/SettingPPM#current','CT.DHF0365/Acquisition#currentAverage';...
    'CT.IDHD0495','CT.DHD0495/SettingPPM#current','CT.DHD0495/Acquisition#current';...
    'CT.IDHD0505','CT.DHD0505/SettingPPM#current','CT.DHD0505/Acquisition#current';...
    'CT.IDHE0255','CT.DHE0255/SettingPPM#current','CT.DHE0255/Acquisition#current';...
    'CT.IDHE0420','CT.DHE0420/SettingPPM#current','CT.DHE0420/Acquisition#current';...
    'CT.IDHE0470','CT.DHE0470/SettingPPM#current','CT.DHE0470/Acquisition#current';...
    'CT.IDHF0612','CT.DHF0612/SettingPPM#current','CT.DHF0612/Acquisition#currentAverage';...
    'CT.IDHF0632','CT.DHF0632/SettingPPM#current','CT.DHF0632/Acquisition#currentAverage';...
    'CT.IDHF0648','CT.DHF0648/SettingPPM#current','CT.DHF0648/Acquisition#currentAverage';...
    'CT.IDHF0658','CT.DHF0658/SettingPPM#current','CT.DHF0658/Acquisition#currentAverage';...
    'CT.IDHF0695','CT.DHF0695/SettingPPM#current','CT.DHF0695/Acquisition#currentAverage';...
    'CT.IDHF0735','CT.DHF0735/SettingPPM#current','CT.DHF0735/Acquisition#currentAverage';...
    'CT.IDHF0755','CT.DHF0755/SettingPPM#current','CT.DHF0755/Acquisition#currentAverage';...
    ...
    'CT.IDVD0160','CT.DVD0160/SettingPPM#current','CT.DVD0160/Acquisition#current';...
    'CT.IDVD0308','CT.DVD0308/SettingPPM#current','CT.DVD0308/Acquisition#currentAverage';...
    'CT.IDVD0360','CT.DVD0360/SettingPPM#current','CT.DVD0360/Acquisition#current';...
    'CT.IDVF0365','CT.DVF0365/SettingPPM#current','CT.DVF0365/Acquisition#currentAverage';...
    'CT.IDVD0495','CT.DVD0495/SettingPPM#current','CT.DVD0495/Acquisition#current';...
    'CT.IDVD0505','CT.DVD0505/SettingPPM#current','CT.DVD0505/Acquisition#current';...
    'CT.IDVE0245','CT.DVE0245/SettingPPM#current','CT.DVE0245/Acquisition#current';...
    'CT.IDVE0410','CT.DVE0410/SettingPPM#current','CT.DVE0410/Acquisition#current';...
    'CT.IDVE0480','CT.DVE0480/SettingPPM#current','CT.DVE0480/Acquisition#current';...
    'CT.IDVF0612','CT.DVF0612/SettingPPM#current','CT.DVF0612/Acquisition#currentAverage';...
    'CT.IDVF0632','CT.DVF0632/SettingPPM#current','CT.DVF0632/Acquisition#currentAverage';...
    'CT.IDVF0648','CT.DVF0648/SettingPPM#current','CT.DVF0648/Acquisition#currentAverage';...
    'CT.IDVF0658','CT.DVF0658/SettingPPM#current','CT.DVF0658/Acquisition#currentAverage';...
    'CT.IDVF0695','CT.DVF0695/SettingPPM#current','CT.DVF0695/Acquisition#currentAverage';...
    'CT.IDVF0735','CT.DVF0735/SettingPPM#current','CT.DVF0735/Acquisition#currentAverage';...
    'CT.IDVF0755','CT.DVF0755/SettingPPM#current','CT.DVF0755/Acquisition#currentAverage';...
    ...
    'CT.IBHE0210-S','CT.BHE0210-S/SettingPPM#current','CT.BHE0210-S/Acquisition#current';...
    'CT.IBHE0540','CT.BHE0540-S/SettingPPM#current','CT.BHE0540-S/Acquisition#current';...
    'CT.IBHF0670','CT.BHF0670-S/SettingPPM#current','CT.BHF0670-S/Acquisition#currentAverage';...
    'CT.ISHC0780','CT.SHC0780-S/SettingPPM#current','CT.SHC0780-S/Acquisition#current';...
    ...
    'CT.IXLC0615','CT.XLC0615/SettingPPM#current','CT.XLC0615/Acquisition#currentAverage';...
    ...
    'CD.IQFF0130','CD.QFF0130-S/SettingPPM#current','CD.QFF0130-S/Acquisition#current';...
    'CD.IQDF0140','CD.QDF0140-S/SettingPPM#current','CD.QDF0140-S/Acquisition#current';...
    'CD.IQFE0170','CD.QFE0170-S/SettingPPM#current','CD.QFE0170-S/Acquisition#current';...
    'CD.IQDF0180','CD.QDF0180-S/SettingPPM#current','CD.QDF0180-S/Acquisition#current';...
    'CD.IQFF0210','CD.QFF0210-S/SettingPPM#current','CD.QFF0210-S/Acquisition#current';...
    'CD.IQFE0230','CD.QFE0230-S/SettingPPM#current','CD.QFE0230-S/Acquisition#current';...
    'CD.IQFF0250','CD.QFF0250-S/SettingPPM#current','CD.QFF0250-S/Acquisition#current';...
    'CD.IQDF0260','CD.QDF0260-S/SettingPPM#current','CD.QDF0260-S/Acquisition#current';...
    'CD.IQDF0280','CD.QDF0280-S/SettingPPM#current','CD.QDF0280-S/Acquisition#current';...
    'CD.IQFF0290','CD.QFF0290-S/SettingPPM#current','CD.QFF0290-S/Acquisition#current';...
    ...
    'CD.IDHE0140','CD.DHE0140/SettingPPM#current','CD.DHE0140/Acquisition#current';...
    'CD.IDHD0185','CD.DHD0185/SettingPPM#current','CD.DHD0185/Acquisition#current';...
    'CD.IDHD0225','CD.DHD0225/SettingPPM#current','CD.DHD0225/Acquisition#current';...
    'CD.IDHE0290','CD.DHE0290/SettingPPM#current','CD.DHE0290/Acquisition#current';...
    'CD.IDHE0350','CD.DHE0350/SettingPPM#current','CD.DHE0350/Acquisition#current';...
    'CD.IDHD0375','CD.DHD0375/SettingPPM#current','CD.DHD0375/Acquisition#current';...
    'CD.IDHD0415','CD.DHD0415-TEST/SettingPPM#current','CD.DHD0415-TEST/Acquisition#currentAverage';...
    'CD.IDHE0460','CD.DHE0460/SettingPPM#current','CD.DHE0460/Acquisition#current';...
    ...
    'CD.IDVD0185','CD.DVD0185/SettingPPM#current','CD.DVD0185/Acquisition#current';...
    'CD.IDVD0225','CD.DVD0225/SettingPPM#current','CD.DVD0225/Acquisition#current';...
    'CD.IDVD0375','CD.DVD0375/SettingPPM#current','CD.DVD0375/Acquisition#current';...
    'CD.IDVD0415','CD.DVD0415/SettingPPM#current','CD.DVD0415/Acquisition#current';...
    'CD.IDVE0280','CD.DVE0280/SettingPPM#current','CD.DVE0280/Acquisition#current';...
    'CD.IDVE0340','CD.DVE0340/SettingPPM#current','CD.DVE0340/Acquisition#current';...
    'CD.IDVE0470','CD.DVE0470/SettingPPM#current','CD.DVE0470/Acquisition#current';...
    ...
    'CD.IBHE0150-S','CD.BHE0150-S/SettingPPM#current','CD.BHE0150-S/Acquisition#current';...
    'CD.ISHA0110','CD.SHA0110-S/SettingPPM#current','CD.SHA0110-S/Acquisition#current';...
    'CD.IWHA0305','CD.WHA0305-A/SettingPPM#current','CD.WHA0305-A/Acquisition#current';...
    'CD.IWHA0305-B','CD.WHA0305-B/SettingPPM#current','CD.WHA0305-B/Acquisition#current';...
    ...
    'CD.IXHA0245','CD.XHA0245-S/SettingPPM#current','CD.XHA0245-S/Acquisition#current';...
    'CD.IXLA0145','CD.XLA0145-S/SettingPPM#current','CD.XLA0145-S/Acquisition#current';...
    'CD.IXVA0215','CD.XVA0215-S/SettingPPM#current','CD.XVA0215-S/Acquisition#current';...
    ...
    'CR.IDHF0145','CR.DHF0145-S/SettingPPM#current','CR.DHF0145-S/Acquisition#currentAverage';...
    'CR.IDHF0200','CR.DHF0200/SettingPPM#current','CR.DHF0200/Acquisition#currentAverage';...
    'CR.IDHF0242','CR.DHF0242/SettingPPM#current','CR.DHF0242/Acquisition#currentAverage';...
    'CR.IDHF0252','CR.DHF0252/SettingPPM#current','CR.DHF0252/Acquisition#currentAverage';...
    'CR.IDHF0292','CR.DHF0292/SettingPPM#current','CR.DHF0292/Acquisition#currentAverage';...
    'CR.IDHF0345','CR.DHF0345/SettingPPM#current','CR.DHF0345/Acquisition#currentAverage';...
    'CR.IDHF0408','CR.DHF0408/SettingPPM#current','CR.DHF0408/Acquisition#currentAverage';...
    'CR.IDHF0452','CR.DHF0452/SettingPPM#current','CR.DHF0452/Acquisition#currentAverage';...
    'CR.IDHF0492','CR.DHF0492/SettingPPM#current','CR.DHF0492/Acquisition#currentAverage';...
    'CR.IDHF0542','CR.DHF0542/SettingPPM#current','CR.DHF0542/Acquisition#currentAverage';...
    'CR.IDHF0582','CR.DHF0582/SettingPPM#current','CR.DHF0582/Acquisition#currentAverage';...
    'CR.IDHF0630','CR.DHF0630/SettingPPM#current','CR.DHF0630/Acquisition#currentAverage';...
    'CR.IDHF0655','CR.DHF0655/SettingPPM#current','CR.DHF0655/Acquisition#currentAverage';...
    'CR.IDHF0700','CR.DHF0700/SettingPPM#current','CR.DHF0700/Acquisition#currentAverage';...
    'CR.IDHF0742','CR.DHF0742/SettingPPM#current','CR.DHF0742/Acquisition#currentAverage';...
    'CR.IDHF0752','CR.DHF0752/SettingPPM#current','CR.DHF0752/Acquisition#currentAverage';...
    'CR.IDHF0792','CR.DHF0792/SettingPPM#current','CR.DHF0792/Acquisition#currentAverage';...
    'CR.IDHF0845','CR.DHF0845/SettingPPM#current','CR.DHF0845/Acquisition#currentAverage';...
    'CR.IDHF0855','CR.DHF0855/SettingPPM#current','CR.DHF0855/Acquisition#currentAverage';...
    'CR.IDHF0908','CR.DHF0908/SettingPPM#current','CR.DHF0908/Acquisition#currentAverage';...
    'CR.IDHF0948','CR.DHF0948/SettingPPM#current','CR.DHF0948/Acquisition#currentAverage';...
    'CR.IDHF0958','CR.DHF0958/SettingPPM#current','CR.DHF0958/Acquisition#currentAverage';...
    'CR.IDHF1000','CR.DHF1000/SettingPPM#current','CR.DHF1000/Acquisition#currentAverage';...
    'CR.IDHF1055','CR.DHF1055/SettingPPM#current','CR.DHF1055/Acquisition#currentAverage';...
    ...
    'CR.IDVF0145','CR.DVF0145/SettingPPM#current','CR.DVF0145/Acquisition#currentAverage';...
    'CR.IDVF0200','CR.DVF0200/SettingPPM#current','CR.DVF0200/Acquisition#currentAverage';...
    'CR.IDVF0242','CR.DVF0242/SettingPPM#current','CR.DVF0242/Acquisition#currentAverage';...
    'CR.IDVF0252','CR.DVF0252/SettingPPM#current','CR.DVF0252/Acquisition#currentAverage';...
    'CR.IDVF0292','CR.DVF0292/SettingPPM#current','CR.DVF0292/Acquisition#currentAverage';...
    'CR.IDVF0345','CR.DVF0345/SettingPPM#current','CR.DVF0345/Acquisition#currentAverage';...
    'CR.IDVF0408','CR.DVF0408/SettingPPM#current','CR.DVF0408/Acquisition#currentAverage';...
    'CR.IDVF0452','CR.DVF0452/SettingPPM#current','CR.DVF0452/Acquisition#currentAverage';...
    'CR.IDVF0492','CR.DVF0492/SettingPPM#current','CR.DVF0492/Acquisition#currentAverage';...
    'CR.IDVF0542','CR.DVF0542/SettingPPM#current','CR.DVF0542/Acquisition#currentAverage';...
    'CR.IDVF0582','CR.DVF0582/SettingPPM#current','CR.DVF0582/Acquisition#currentAverage';...
    'CR.IDVF0630','CR.DVF0630/SettingPPM#current','CR.DVF0630/Acquisition#currentAverage';...
    'CR.IDVF0655','CR.DVF0655/SettingPPM#current','CR.DVF0655/Acquisition#currentAverage';...
    'CR.IDVF0700','CR.DVF0700/SettingPPM#current','CR.DVF0700/Acquisition#currentAverage';...
    'CR.IDVF0742','CR.DVF0742/SettingPPM#current','CR.DVF0742/Acquisition#currentAverage';...
    'CR.IDVF0752','CR.DVF0752/SettingPPM#current','CR.DVF0752/Acquisition#currentAverage';...
    'CR.IDVF0792','CR.DVF0792/SettingPPM#current','CR.DVF0792/Acquisition#currentAverage';...
    'CR.IDVF0845','CR.DVF0845/SettingPPM#current','CR.DVF0845/Acquisition#currentAverage';...
    'CR.IDVF0855','CR.DVF0855/SettingPPM#current','CR.DVF0855/Acquisition#currentAverage';...
    'CR.IDVF0908','CR.DVF0908/SettingPPM#current','CR.DVF0908/Acquisition#currentAverage';...
    'CR.IDVF0948','CR.DVF0948/SettingPPM#current','CR.DVF0948/Acquisition#currentAverage';...
    'CR.IDVF0958','CR.DVF0958/SettingPPM#current','CR.DVF0958/Acquisition#currentAverage';...
    'CR.IDVF1000','CR.DVF1000/SettingPPM#current','CR.DVF1000/Acquisition#currentAverage';...
    'CR.IDVF1055','CR.DVF1055/SettingPPM#current','CR.DVF1055/Acquisition#currentAverage';...
    ...
    'CR.IQFG0120','CR.QFG0120-S/SettingPPM#current','CR.QFG0120-S/Acquisition#currentAverage';...
    'CR.IQDG0140','CR.QDG0140-S/SettingPPM#current','CR.QDG0140-S/Acquisition#currentAverage';...
    'CR.IQDF0160','CR.QDF0160-S/SettingPPM#current','CR.QDF0160-S/Acquisition#currentAverage';...
    'CR.IQFF0190','CR.QFF0190-S/SettingPPM#current','CR.QFF0190-S/Acquisition#currentAverage';...
    'CR.IQFJ0215','CR.QFJ0215-S/SettingPPM#current','CR.QFJ0215-S/Acquisition#currentAverage';...
    'CR.IQDJ0230','CR.QDJ0230-S/SettingPPM#current','CR.QDJ0230-S/Acquisition#currentAverage';...
    'CR.IQFJ0245','CR.QFJ0245-S/SettingPPM#current','CR.QFJ0245-S/Acquisition#currentAverage';...
    'CR.IQFJ0320','CR.QFJ0320-S/SettingPPM#current','CR.QFJ0320-S/Acquisition#currentAverage';...
    'CR.IQDH0340','CR.QDH0340-S/SettingPPM#current','CR.QDH0340-S/Acquisition#currentAverage';...
    'CR.IQFF0510','CR.QFF0510-S/SettingPPM#current','CR.QFF0510-S/Acquisition#currentAverage';...
    'CR.IQDF0540','CR.QDF0540-S/SettingPPM#current','CR.QDF0540-S/Acquisition#currentAverage';...
    'CR.IQDG0560','CR.QDG0560-S/SettingPPM#current','CR.QDG0560-S/Acquisition#currentAverage';...
    'CR.IQFG0580','CR.QFG0580-S/SettingPPM#current','CR.QFG0580-S/Acquisition#currentAverage';...
    'CR.IQFJ0715','CR.QFJ0715-S/SettingPPM#current','CR.QFJ0715-S/Acquisition#currentAverage';...
    'CR.IQDJ0730','CR.QDJ0730-S/SettingPPM#current','CR.QDJ0730-S/Acquisition#currentAverage';...
    'CR.IQFJ0745','CR.QFJ0745-S/SettingPPM#current','CR.QFJ0745-S/Acquisition#currentAverage';...
    'CR.IQFJ0820','CR.QFJ0820-S/SettingPPM#current','CR.QFJ0820-S/Acquisition#currentAverage';...
    'CR.IQDH0840','CR.QDH0840-S/SettingPPM#current','CR.QDH0840-S/Acquisition#currentAverage';...
    ...
    'CR.IBHF0205','CR.BHF0205-S/SettingPPM#current','CR.BHF0205-S/Acquisition#current';...
    'CR.IWHA350A','CR.WHA0350-A/SettingPPM#current','CR.WHA0350-A/Acquisition#currentAverage';...
    'CR.IWHA350B','CR.WHA0350-B/SettingPPM#current','CR.WHA0350-B/Acquisition#currentAverage';...
    'CR.IKHA0550','CR.KHA0550/SettingPPM#current','CR.KHA0550/Acquisition#current';...
    ...
    'CR.IXLB0210','CR.XLB0210-S/SettingPPM#current','CR.XLB0210-S/Acquisition#currentAverage';...
    'CR.IXHB0220','CR.XHB0220-S/SettingPPM#current','CR.XHB0220-S/Acquisition#currentAverage';...
    'CR.IXVB0240','CR.XVB0240-S/SettingPPM#current','CR.XVB0240-S/Acquisition#currentAverage';...
    ...
    'CC.IDHD0125','CC.DHD0125/SettingPPM#current','CC.DHD0125/Acquisition#currentAverage';...
    'CC.IDHD0175','CC.DHF0175/SettingPPM#current','CC.DHF0175/Acquisition#currentAverage';...
    'CC.IDHD0225','CC.DHF0225/SettingPPM#current','CC.DHF0225/Acquisition#currentAverage';...
    'CC.IDHD0265','CC.DHF0265/SettingPPM#current','CC.DHF0265/Acquisition#currentAverage';...
    'CC.IDHD0345','CC.DHF0345/SettingPPM#current','CC.DHF0345/Acquisition#currentAverage';...
    'CC.IDHD0435','CC.DHF0435/SettingPPM#current','CC.DHF0435/Acquisition#currentAverage';...
    'CC.IDHD0465','CC.DHF0465/SettingPPM#current','CC.DHF0465/Acquisition#currentAverage';...
    'CC.IDHD0525','CC.DHF0525/SettingPPM#current','CC.DHF0525/Acquisition#currentAverage';...
    'CC.IDHD0615','CC.DHF0615/SettingPPM#current','CC.DHF0615/Acquisition#currentAverage';...
    'CC.IDHD0655','CC.DHF0655/SettingPPM#current','CC.DHF0655/Acquisition#currentAverage';...
    'CC.IDHD0765','CC.DHF0765/SettingPPM#current','CC.DHF0765/Acquisition#currentAverage';...
    'CC.IDHD0855','CC.DHF0855/SettingPPM#current','CC.DHF0855/Acquisition#currentAverage';...
    'CC.IDHD0940','CC.DHD0940/SettingPPM#current','CC.DHD0940/Acquisition#currentAverage';...
    'CC.IDVD0125','CC.DVD0125/SettingPPM#current','CC.DVD0125/Acquisition#currentAverage';...
    'CC.IDVD0175','CC.DVF0175/SettingPPM#current','CC.DVF0175/Acquisition#currentAverage';...
    'CC.IDVD0225','CC.DVF0225/SettingPPM#current','CC.DVF0225/Acquisition#currentAverage';...
    'CC.IDVD0265','CC.DVF0265/SettingPPM#current','CC.DVF0265/Acquisition#currentAverage';...
    'CC.IDVD0345','CC.DVF0345/SettingPPM#current','CC.DVF0345/Acquisition#currentAverage';...
    'CC.IDVD0435','CC.DVF0435/SettingPPM#current','CC.DVF0435/Acquisition#currentAverage';...
    'CC.IDVD0465','CC.DVF0465/SettingPPM#current','CC.DVF0465/Acquisition#currentAverage';...
    'CC.IDVD0525','CC.DVF0525/SettingPPM#current','CC.DVF0525/Acquisition#currentAverage';...
    'CC.IDVD0615','CC.DVF0615/SettingPPM#current','CC.DVF0615/Acquisition#currentAverage';...
    'CC.IDVD0655','CC.DVF0655/SettingPPM#current','CC.DVF0655/Acquisition#currentAverage';...
    'CC.IDVD0765','CC.DVF0765/SettingPPM#current','CC.DVF0765/Acquisition#currentAverage';...
    'CC.IDVD0855','CC.DVF0855/SettingPPM#current','CC.DVF0855/Acquisition#currentAverage';...
    'CC.IDVD0940','CC.DVD0940/SettingPPM#current','CC.DVD0940/Acquisition#currentAverage';...
    
    'CC.IQFG0140','CC.QFG0140/SettingPPM#current','CC.QFG0140/Acquisition#currentAverage';...
    'CC.IQDG0160','CC.QDG0160/SettingPPM#current','CC.QDG0160/Acquisition#currentAverage';...
    'CC.IQFH0180','CC.QFH0180/SettingPPM#current','CC.QFH0180/Acquisition#currentAverage';...
    'CC.IQFH0210','CC.QFH0210/SettingPPM#current','CC.QFH0210/Acquisition#currentAverage';...
    'CC.IQDH0220','CC.QDH0220/SettingPPM#current','CC.QDH0220/Acquisition#currentAverage';...
    'CC.IQFH0230','CC.QFH0230/SettingPPM#current','CC.QFH0230/Acquisition#currentAverage';...
    'CC.IQFL0270','CC.QFL0270/SettingPPM#current','CC.QFL0270/Acquisition#currentAverage';...
    'CC.IQDL0280','CC.QDL0280/SettingPPM#current','CC.QDL0280/Acquisition#currentAverage';...
    'CC.IQDL0330','CC.QDL0330-S/SettingPPM#current','CC.QDL0330-S/Acquisition#currentAverage';...
    'CC.IQFH0350','CC.QFH0350/SettingPPM#current','CC.QFH0350/Acquisition#currentAverage';...
    ...%'CC.IQDH0370','CC.QDH0370/SettingPPM#current','CC.QDH0370/Acquisition#currentAverage';...
    'CC.IQDL0430','CC.QDL0430/SettingPPM#current','CC.QDL0430/Acquisition#currentAverage';...
    'CC.IQFL0450','CC.QFL0450/SettingPPM#current','CC.QFL0450/Acquisition#currentAverage';...
    ...%'CC.IQDL0470','CC.QDL0470/SettingPPM#current','CC.QDL0470/Acquisition#currentAverage';...
    'CC.IQDH0490','CC.QDH0490/SettingPPM#current','CC.QDH0490/Acquisition#currentAverage';...
    'CC.IQFL0530','CC.QFL0530/SettingPPM#current','CC.QFL0530/Acquisition#currentAverage';...
    'CC.IQDL0550','CC.QDL0550/SettingPPM#current','CC.QDL0550/Acquisition#currentAverage';...
    'CC.IQFL0570','CC.QFL0570/SettingPPM#current','CC.QFL0570/Acquisition#currentAverage';...
    'CC.IQFL0620','CC.QFL0620/SettingPPM#current','CC.QFL0620/Acquisition#currentAverage';...
    'CC.IQDL0650','CC.QDL0650/SettingPPM#current','CC.QDL0650/Acquisition#currentAverage';...
    'CC.IQFL0680','CC.QFL0680/SettingPPM#current','CC.QFL0680/Acquisition#currentAverage';...
    'CC.IQFL0730','CC.QFL0730/SettingPPM#current','CC.QFL0730/Acquisition#currentAverage';...
    'CC.IQDL0750','CC.QDL0750/SettingPPM#current','CC.QDL0750/Acquisition#currentAverage';...
    'CC.IQDH0790','CC.QDH0790/SettingPPM#current','CC.QDH0790/Acquisition#currentAverage';...
    'CC.IQDD0820','CC.QDD0820/SettingPPM#current','CC.QDD0820/Acquisition#currentAverage';...
    'CC.IQFD0840','CC.QFD0840/SettingPPM#current','CC.QFD0840/Acquisition#currentAverage';...
    'CC.IQFL0910','CC.QFL0910/SettingPPM#current','CC.QFL0910/Acquisition#currentAverage';...
    'CC.IQDL0920','CC.QDL0920/SettingPPM#current','CC.QDL0920/Acquisition#currentAverage';...
    ...
    'CC.IBVA0300','CC.BVA0300-S/SettingPPM#current','CC.BVA0300-S/Acquisition#currentAverage';...
    'CC.IBVB0400','CC.BVB0400/SettingPPM#current','CC.BVB0400/Acquisition#currentAverage';...
    'CC.IBHH0200','CC.BHH0200/SettingPPM#current','CC.BHH0200/Acquisition#currentAverage';...
    'CC.IBHG0500','CC.BHG0500-S/SettingPPM#current','CC.BHG0500-S/Acquisition#currentAverage';...
    'CC.IBHH0600','CC.BHH0600-S/SettingPPM#current','CC.BHH0600-S/Acquisition#currentAverage';...
    ...
    'CM.IDHD0240','CM.DHD0240/SettingPPM#current','CM.DHD0240/Acquisition#currentAverage';...
    'CM.IDHD0290','CM.DHD0290/SettingPPM#current','CM.DHD0290/Acquisition#currentAverage';...
    'CM.IDHJ0340','CM.DHJ0340/SettingPPM#current','CM.DHJ0340/Acquisition#currentAverage';...
    'CM.IDHJ0360','CM.DHJ0360/SettingPPM#current','CM.DHJ0360/Acquisition#currentAverage';...
    'CM.IDHJ0520','CM.DHJ0520/SettingPPM#current','CM.DHJ0520/Acquisition#currentAverage';...
    'CM.IDHJ0540','CM.DHJ0540/SettingPPM#current','CM.DHJ0540/Acquisition#currentAverage';...
    ...
    'CM.IDVD0240','CM.DVD0240/SettingPPM#current','CM.DVD0240/Acquisition#currentAverage';...
    'CM.IDVD0290','CM.DVD0290/SettingPPM#current','CM.DVD0290/Acquisition#currentAverage';...
    'CM.IDVJ0340','CM.DVJ0340/SettingPPM#current','CM.DVJ0340/Acquisition#currentAverage';...
    'CM.IDVJ0360','CM.DVJ0360/SettingPPM#current','CM.DVJ0360/Acquisition#currentAverage';...
    'CM.IDVJ0520','CM.DVJ0520/SettingPPM#current','CM.DVJ0520/Acquisition#currentAverage';...
    'CM.IDVJ0540','CM.DVJ0540/SettingPPM#current','CM.DVJ0540/Acquisition#currentAverage';...
    ...
    'CM.IQFN0130','CM.QFN0130-S/SettingPPM#current','CM.QFN0130-S/Acquisition#currentAverage';...
    'CM.IQDH0140','CM.QDH0140/SettingPPM#current','CM.QDH0140/Acquisition#currentAverage';...
    'CM.IQFP0220','CM.QFP0220/SettingPPM#current','CM.QFP0220/Acquisition#currentAverage';...
    'CM.IQDP0225','CM.QDP0225/SettingPPM#current','CM.QDP0225/Acquisition#currentAverage';...
    'CM.IQFQ0260','CM.QFQ0260/SettingPPM#current','CM.QFQ0260/Acquisition#currentAverage';...
    'CM.IQDQ0265','CM.QDQ0265/SettingPPM#current','CM.QDQ0265/Acquisition#currentAverage';...
    'CM.IQFD0310','CM.QFD0310/SettingPPM#current','CM.QFD0310/Acquisition#currentAverage';...
    'CM.IQDD0315','CM.QDD0315/SettingPPM#current','CM.QDD0315/Acquisition#currentAverage';...
    'CM.IQFD0320','CM.QFD0320/SettingPPM#current','CM.QFD0320/Acquisition#currentAverage';...
    'CM.IQFD0560','CM.QFD0560/SettingPPM#current','CM.QFD0560/Acquisition#currentAverage';...
    'CM.IQDD0565','CM.QDD0565/SettingPPM#current','CM.QDD0565/Acquisition#currentAverage';...
    'CM.IQFD0570','CM.QFD0570/SettingPPM#current','CM.QFD0570/Acquisition#currentAverage';...
    ...
    'CM.IBHL0100','CM.BHL0100/SettingPPM#current','CM.BHL0100/Acquisition#currentAverage';...
    'CM.IBHL0200','CM.BHL0200/SettingPPM#current','CM.BHL0200/Acquisition#currentAverage';...
    'CM.IBHB600','CM.BHB600/SettingPPM#current','CM.BHB600/Acquisition#currentAverage';...
    };

output=containers.Map();
fields = {'madXName', 'setSignal', 'getSignal'};

% DEBUG DURING SHOUTDOWN
%disp('WARNING(ctf3elementsDatabaseGenerator):: I only use the CCV value signal both for acquisition and set!');
%data(:,3) = data(:,2);

for i=1:size(data,1)
    aux=data(i,:);
    % by convention the madx name is always uppercase (madx doesn't pay
    % attention acctually)...
    aux(1)=upper(aux(1)); 
    output(cell2mat(data(i,1)))=cell2struct(aux,fields,2);
end
end

