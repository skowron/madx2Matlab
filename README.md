# madx2matlab

Simple and easy to use MATLAB interface to MAD-X.

It allows to:
- execute MAD-X via stdin/stdout without creating unecessary files.
- load TFS tables from file into MATLAB structures

Known issues:
- slow: it keeps parsing strings...
