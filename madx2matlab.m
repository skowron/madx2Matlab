classdef madx2matlab<handle
    %MADX2MATLAB Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % conversion database to be able to read/set machine parameters
        % it must have those fields:
        %           conversionMapDatabase.madXName
        %           conversionMapDatabase.setSignal
        %           conversionMapDatabase.getSignal
        conversionMapDatabase = containers.Map();
        
        % files in use
        madxScriptFile
        madxParametersFile
        madxExecutable = './madx';
        
        % madx script interesting content lines
        % cell array
        madxScriptLines
        
        % madx matching script content lines
        % it is a cell array
        madxMatchingLines
        
        % madx parameters that can be set for the current script
        % struct array with fields "name", "value" and "comment"
        madxParametersMap
        
        % madx misalignments of elements. for each pattern the user will
        % define, it will be associated a struct with fields:
        %  "pattern", "dx", "dy", "ds", "dphi", "dtheta", "dpsi", "mrex", "mrey"
        misalignmentsMap
        
        % madx field errors. For each "pattern" the user can define a
        % struct with fields:
        %  dkn, dks, dknr, dksr
        % each of which can be a vector giving the field error components
        % at the different orders.
        fieldErrorsMap
        
        % when is getting/setting data from the machine, use this cycle
        machineCycleName='SCT.USER.SETUP';
        
        % twiss command line
        % an example may be:
        %   TWISS,   BETX=real,ALFX=real,MUX=real,
        %          BETY=real,ALFY=real,MUY=real,
        %          DX=real,DPX=real,DY=real,DPY=real,
        %          X=real,PX=real,Y=real,PY=real,
        %          T=real,PT=real,
        %          WX=real,PHIX=real,DMUX=real,
        %          WY=real,PHIY=real,DMUY=real,
        %          DDX=real,DDY=real,DDPX=real,DDPY=real,
        %          R11=real,R12=real,R21=real,R22=real,  !coupling matrix
        %          TABLE=table_name,
        %          TOLERANCE=real,
        %          DELTAP=real:real:real;
        twissCommandLine = {'twiss;'};
        twissGetOutputCommandLine = {'write, table=twiss;'};
        
        % survey command line
        % an example may be:
        %  SURVEY, x0=double, y0=double, z0=double,
        %         theta0=double, phi0=double, psi0=double,
        %         file=string, table=string, sequence=string;
        %
        %         parameter   meaning                                     default value
        %           x0 	      initial horizontal transverse coordinate    0.0
        %           y0          initial vertical   transverse coordinate    0.0
        %           z0          initial longitudinal coordinate             0.0
        %           theta0      initial horizontal angle                    0.0
        %           phi0        initial vertical angle                      0.0
        %           psi0        initial transverse tilt                     0.0
        %           file        name of external file                       null (default name survey)
        %           table       name of internal table                      null (ignored, always table='survey')
        %           sequence    name of sequence to be surveyed             last used sequence
        surveyCommandLine = {'survey;'};
        surveyGetOutputCommandLine = {'write, table=survey;'};
        
        
        % track command
        % It is important here to add ad the end what you want to have as
        % output. In the default it is: "write, table=trackone;", but maybe
        % you want a different table as track..
        trackCommandLine = [...
            'TRACK, deltap = 0, onepass, onetable;',char(10),...
            'START, x = 0, px = 0, y = 0, py = 0, t = 0, pt = 0;',char(10),...
            'RUN;',char(10),...
            'ENDTRACK;',char(10),...
            ];
        % track output command line:
        % since different tracking puts different things in different
        % tables, I prefer here to give the user the possibility to specify
        % a custom command to print the track table.
        trackGetOutputCommandLine = 'write, table=trackone;';
        
        % aperture command line.
        % the default options should be fine for LHC.
        apertureCommandLine = {'aperture;'};
        % aperture output command line:
        apertureGetOutputCommandLine = {'write, table=aperture;'};
    
        
        % twiss and survey output format
        madxFormat='1.17f';
        
        % debug set it to see MADX commands
        debugMADX = false;
        
    end
    
    methods
        function obj=madx2matlab(madxScriptFile, madxParametersFile, madxExecutable)
            % obj=madx2matlab(madxScriptFile [, madxParametersFile [, madxExecutable]])
            % It create a madx2matlab object.
            % Inputs are:
            %   * madxScriptFile (i.e. something that eventually defines/loads a sequence)
            %   * madxParametersFile (i.e. the equivalent of a typical
            %   current file used in MADX scripts. Each of the parameter
            %   here defined will be editable afterwards, such that the
            %   computed optics will take into account the new
            %   parameters/currents. It can be omitted.)
            %   * madxExecutable (i.e. the MADX executable file. If omitted
            %   it will try to use a local ./madx executable)
            
            if nargin == 0
                madxScriptFile = '';
                madxParametersFile = '';
                madxExecutable = './madx';
            elseif nargin == 1
                madxParametersFile = '';
                madxExecutable = './madx';
            elseif nargin == 2
                madxExecutable = './madx';
            end
            
            % save files reference
            obj.madxScriptFile = madxScriptFile;
            obj.madxParametersFile = madxParametersFile;
            obj.madxExecutable = madxExecutable;
            
            % read files
            obj.madxScriptLines = madx2matlab.parseScriptFile(obj.madxScriptFile);
            obj.madxParametersMap = madx2matlab.parseParametersFile(obj.madxParametersFile);
            
            % define misalignments Map
            obj.misalignmentsMap=containers.Map();
            % define fields errors Map
            obj.fieldErrorsMap=containers.Map();
        end
        
        function X=getModelParameters(obj, madxParametersList)
            % X=getModelParameters(obj, madxParametersList)
            % It returns the model parameter values of the requested list.
            
            madxParametersList = upper(madxParametersList);
            
            if ~iscell(madxParametersList)
                madxParametersList={madxParametersList};
            end
            X = NaN(length(madxParametersList),1);
            for i=1:length(madxParametersList)
                try
                    X(i) = obj.madxParametersMap(madxParametersList{i}).value;
                catch e
                    disp(['No parameter "',madxParametersList{i},'" defined in current model. Check your code!']);
                    continue
                end
            end
        end
        
        function setModelParameters(obj, madxParametersList, X)
            % setModelParameters(obj, madxParametersList, X)
            % it sets the model parameters (e.g. magnet currents defined in
            % the initial "madxParametersFile") to the wanted value.
            %
            % If you set a parameter to NaN, this is removed from the
            % parameter list.
            
            madxParametersList = upper(madxParametersList);
            
            if ~iscell(madxParametersList)
                madxParametersList = {madxParametersList};
            end
            
            if length(madxParametersList) ~= length(X)
                error('"madxParametersList" and "X" arguments must have the same length!')
            end
            
            for i=1:length(madxParametersList)
                if isnan(X(i))
                    obj.madxParametersMap.remove(madxParametersList{i});
                else
                    try
                        tmpStruct = obj.madxParametersMap(madxParametersList{i});
                    catch e
                        disp(['At the moment there is no ',madxParametersList{i}, ' defined in this madx2matlab object. I will define it now...']);
                        tmpStruct = struct;
                        tmpStruct.name=madxParametersList{i};
                        tmpStruct.comment='Parameter defined using madx2matlab class!';
                    end
                    tmpStruct.value = X(i);
                    obj.madxParametersMap(madxParametersList{i}) = tmpStruct;
                end
            end
        end
        
     
        function saveModelParametersToFile(obj, filename)
            % saveModelParametersToFile(obj, filename)
            % It will dump all the model parameters with their current
            % values to the 'filename' specified.
            %
            % WARNING! this will overwrite your file if it already exist.
            %
            
            madx2matlab.writeParametersMapToFile(obj.madxParametersMap, filename);
        end
        
        
        function X=getMachineParameters(obj, madxParametersList, fromCCVvalue)
            % X=getMachineParameters(obj, madxParametersList, fromCCVvalue)
            % It tries to read the wanted parameters from the real machine.
            %
            % WARNING:
            %   * This code has been developed for CTF3, so the default
            %   cycle used to get parameters is machineCycleName='SCT.USER.SETUP';
            %   Of course one can define a different one changing the value
            %   of the machineCycleName parameter of this object.
            %
            % IMPORTANT:
            %   * Normally there is no 1to1 relationship between a MADX model
            %   parameter name and the device/property#field used in the
            %   accelerator control system. In order to translate the MADX
            %   parameter names to real machine devices, one has to provide
            %   an "elements database".
            %   Please take as example the script
            %   "ctf3elementsDatabaseGenerator.m" that generate this
            %   database for CTF3. You should provide this, or your own,
            %   database in this object parameter "madxParametersMap".
            %
            % if "fromCCVvalue" = true, then it will get the CCV value of
            % the given parameter instead of the "AQN" value. By default it
            % takes the "AQN" value!
            
            if nargin == 2
                fromCCVvalue = false;
            end
            
            madxParametersList = upper(madxParametersList);
            
            if ~iscell(madxParametersList)
                madxParametersList = {madxParametersList};
            end
            
            X=NaN(length(madxParametersList),1);
            for i=1:length(madxParametersList)
                try
                    parameterStruct=obj.conversionMapDatabase(upper(madxParametersList{i}));
                catch e
                    disp(['Error extracting get signal for ',upper(madxParametersList{i}),': ',e.message])
                    disp(['No information about machine signal to read ',upper(madxParametersList{i}),...
                        '. Please check your code and/or add this information in the MapDatabase!']);
                    continue
                end
                
                
                if isempty(parameterStruct)
                    disp(['No information about machine signal to read ',upper(madxParametersList{i}),...
                        '. Please check your code and/or add this information in the MapDatabase!']);
                    continue
                else
                    if fromCCVvalue
                        auxSignal = parameterStruct.setSignal;
                    else
                        auxSignal = parameterStruct.getSignal;
                    end
                    try
                        X(i)=matlabJapc.staticGetSignal(obj.machineCycleName, auxSignal);
                    catch exception
                        disp(['Unable to read a good value for parameter ',parameterStruct.madXName,' (signal: ',auxSignal,') from the machine: ', exception.message]);
                    end
                end
            end
        end
        
        function setAllModelParametersFromMachine(obj, fromCCVvalue)
            % setAllModelParametersFromMachine(obj)
            % It tries to read all the madx model parameters (as you
            % defined in madxParametersFile) from the real machine.
            % In order to do so, you must have provided a proper database
            % to convert madxParameters in devices defined in the
            % accelerator control system.
            %
            % if "fromCCVvalue" = true, then it will get the CCV value of
            % the given parameter instead of the "AQN" value. By default it
            % takes the "AQN" value!
            %
            % (see help of "getMachineParameters" function!)
            
            % default value for inputs
            if nargin == 1
                fromCCVvalue = false;
            end
            
            % cycle on all parameters
            allParametersNames = obj.madxParametersMap.keys;
            for i=1:length(allParametersNames)
                value = obj.getMachineParameters(allParametersNames(i), fromCCVvalue);
                if isfinite(value)
                    obj.setModelParameters(allParametersNames(i), value);
                else
                    disp(['Unable to read value for ',allParametersNames{i}]);
                    disp(['Keeping previous value: ', num2str(obj.madxParametersMap(allParametersNames{i}).value)]);
                end
            end
        end
        
        function setMachineParameters(obj, madxParametersList, X)
            % setMachineParameters(obj, madxParametersList, X)
            %
            % WARNING!!!
            % THIS FUNCTION WILL ACTUALLY CHANGE THE REAL MACHINE!
            %
            % DEPRECATED!!!
            % too dangerous to leave it in this simulation code.
            % It is not doing anything anymore.
            %
            
            % deprecated. But leave the old code inside %%%%%%%%%%%%%%%%%%%
            error('Setting machine parameters is deprecated at the moment.')
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % OLD CODE: %%%%%
            
            madxParametersList = upper(madxParametersList);
            
            for i=1:length(madxParametersList)
                try
                    parameterStruct=obj.conversionMapDatabase(madxParametersList{i});
                catch e
                    disp(['Error extracting set signal for ',madxParametersList{i},': ',e.message])
                    disp(['No information about machine signal to set ',madxParametersList{i},...
                        '. Please check your code or add this information in the MapDatabase!']);
                    continue
                end
                if isempty(parameterStruct) || isempty(parameterStruct.setSignal)
                    disp(['No information about machine signal to set ',madxParametersList{i},...
                        '. Please check your code or add this information in the MapDatabase!']);
                    continue
                else
                    % OLD CODE. just to be sure.
                    %matlabJapc.staticSetSignal(obj.machineCycleName, parameterStruct.setSignal, X(i));
                end
            end
        end
        
        function setMisalignment(obj, pattern, dx, dy, ds, dphi, dtheta, dpsi, mrex, mrey)
            % setMisalignment(obj, pattern, dx, dy, ds, dphi, dtheta, dpsi, mrex, mrey)
            % With this function one can define misalignments of the madx
            % elements.
            % The misalignments will be set before any twiss/track command
            % you will require, i.e. just before computing the optics
            % function if you run the computeOptics function.
            %
            
            % check arguments
            if nargin < 3
                error('madx2matlab::setMisalignment: at least dx misalignment has to be provided. Order: setMisalignment(obj, pattern, dx, <dy>, <ds>, <dphi>, <dtheta>, <dpsi>, <mrex>, <mrey>)');
            end
            if nargin < 4
                dy = 0;
            end
            if nargin < 5
                ds = 0;
            end
            if nargin < 6
                dphi = 0;
            end
            if nargin < 7
                dtheta = 0;
            end
            if nargin < 8
                dpsi = 0;
            end
            if nargin < 9
                mrex = 0;
            end
            if nargin < 10
                mrey = 0;
            end
            
                        
            % see if we alrady have a misalignment for this element
            if obj.misalignmentsMap.isKey(pattern)
                warning(['Misalignment already exists for ', pattern, '. Will ADD the new one as well.']);
                auxOldMis = obj.misalignmentsMap(pattern);
                dx = dx + auxOldMis.dx;
                dy = dy + auxOldMis.dy;
                ds = ds + auxOldMis.ds;
                dphi = dphi + auxOldMis.dphi;
                dtheta = dtheta + auxOldMis.dtheta;
                dpsi = dpsi + auxOldMis.dpsi;
                mrex = mrex + auxOldMis.mrex;
                mrey = mrey + auxOldMis.mrey;
            end
            
            % create struct
            tmpStruct = struct;
            tmpStruct.pattern = pattern;
            tmpStruct.dx = dx;
            tmpStruct.dy = dy;
            tmpStruct.ds = ds;
            tmpStruct.dphi = dphi;
            tmpStruct.dtheta = dtheta;
            tmpStruct.dpsi = dpsi;
            tmpStruct.mrex = mrex;
            tmpStruct.mrey = mrey;
            % add it to map
            obj.misalignmentsMap(tmpStruct.pattern) = tmpStruct;
        end
        
        function removeMisalignment(obj, pattern)
            % removeMisalignment(obj, [pattern])
            % It just removes the misalignment specified for one pattern.
            % If no pattern is specified, all the misalignments will be
            % lost.
            if nargin == 1
                disp('Removing all misalignments data...')
                obj.misalignmentsMap = containers.Map();
            else
                obj.misalignmentsMap.remove(pattern);
            end
        end
        
        function setFieldError(obj, pattern, dkn, dks, dknr, dksr)
            % setFieldError(obj, pattern, dkn, dks, dknr, dksr)
            % Similar to setMisalignment function, but for field errors.
            
            % check arguments
            if nargin < 3
                error('madx2matlab::setFieldError: at least a pattern and dkn field error has to be provided.');
            elseif nargin < 4
                dks = [];
                dknr = [];
                dksr = [];
            elseif nargin < 5
                dknr = [];
                dksr = [];
            elseif nargin < 6
                dksr = [];
            end
            
            
            
            % see if we alrady have a misalignment for this element
            if obj.fieldErrorsMap.isKey(pattern)
                warning(['Field error already exists for ', pattern, '. Will ADD the new one as well.']);
                
                auxOldErr = obj.fieldErrorsMap(pattern);
                %
                if ~isempty(auxOldErr.dkn)
                    if ~isempty(dkn)
                        [~, auxMaxIdx] = max([length(dkn), length(auxOldErr.dkn)]);
                        if auxMaxIdx == 1
                            for k=1:length(auxOldErr.dkn)
                                dkn(k) = dkn(k)+auxOldErr.dkn(k);
                            end
                        else
                            for k=1:length(auxOldErr.dkn)
                                auxOldErr.dkn(k) = auxOldErr.dkn(k)+dkn(k);
                            end
                            dkn = auxOldErr.dkn;
                        end
                    else
                        dkn = auxOldErr.dkn;
                    end
                end
                %dks
                if ~isempty(auxOldErr.dks)
                    if ~isempty(dks)
                        [~, auxMaxIdx] = max([length(dks), length(auxOldErr.dks)]);
                        if auxMaxIdx == 1
                            for k=1:length(auxOldErr.dks)
                                dks(k) = dks(k)+auxOldErr.dks(k);
                            end
                        else
                            for k=1:length(auxOldErr.dks)
                                auxOldErr.dks(k) = auxOldErr.dks(k)+dks(k);
                            end
                            dks = auxOldErr.dks;
                        end
                    else
                        dks = auxOldErr.dks;
                    end
                end
                %dknr
                if ~isempty(auxOldErr.dknr)
                    if ~isempty(dknr)
                        [~, auxMaxIdx] = max([length(dknr), length(auxOldErr.dknr)]);
                        if auxMaxIdx == 1
                            for k=1:length(auxOldErr.dknr)
                                dknr(k) = dknr(k)+auxOldErr.dknr(k);
                            end
                        else
                            for k=1:length(auxOldErr.dknr)
                                auxOldErr.dknr(k) = auxOldErr.dknr(k)+dknr(k);
                            end
                            dknr = auxOldErr.dknr;
                        end
                    else
                        dknr = auxOldErr.dknr;
                    end
                end
                %dksr
                if ~isempty(auxOldErr.dksr)
                    if ~isempty(dksr)
                        [~, auxMaxIdx] = max([length(dksr), length(auxOldErr.dksr)]);
                        if auxMaxIdx == 1
                            for k=1:length(auxOldErr.dksr)
                                dksr(k) = dksr(k)+auxOldErr.dksr(k);
                            end
                        else
                            for k=1:length(auxOldErr.dkn)
                                auxOldErr.dkn(k) = auxOldErr.dksr(k)+dksr(k);
                            end
                            dksr = auxOldErr.dksr;
                        end
                    else
                        dksr = auxOldErr.dksr;
                    end
                end
            end
            

            % just some warning
            if ~isempty(dkn) && ~isempty(dknr)
                warning(['You provided both absolute and relative errors for ',pattern,char(10),...
                    ' Only absolute errors will be used.']);
            end
            if ~isempty(dks) && ~isempty(dksr)
                warning(['You provided both absolute and relative skew errors for ',pattern,char(10),...
                    ' Only absolute errors will be used.']);
            end
            
            % create struct
            tmpStruct = struct;
            tmpStruct.pattern = pattern;
            tmpStruct.dkn = dkn;
            tmpStruct.dks = dks;
            tmpStruct.dknr = dknr;
            tmpStruct.dksr = dksr;
            % add it to map
            obj.fieldErrorsMap(tmpStruct.pattern) = tmpStruct;
        end
        
        function removeFieldError(obj, pattern)
            % removeFieldError(obj, [pattern])
            % It just removes the fields error specified for one pattern.
            % If no pattern is specified, all the errors will be
            % lost.
            if nargin == 1
                disp('Removing all field error data...')
                obj.fieldErrorsMap = containers.Map();
            else
                obj.fieldErrorsMap.remove(pattern);
            end
        end
        
        function setGenericMachineImperfection(obj, madxElementName, elementErrorType, errorValue)
            % setSingleMachineImperfection(obj, madxElementName, elementErrorType, errorValue)
            %
            % this function allows to set any imperfection type
            % (misalignements and field errros)
            % for an element.
            %
            % TODO: make it more general to allow many errors to be set at
            % the same time.
            % 
            
            switch elementErrorType
                case {'DX','dx'}
                    obj.setMisalignment(madxElementName, ...
                        errorValue, 0, 0,...
                        0, 0, 0);
                case {'DY','dy'}
                    obj.setMisalignment(madxElementName, ...
                        0, errorValue, 0,...
                        0, 0, 0);
                case {'DS','ds'}
                    obj.setMisalignment(madxElementName, ...
                        0, 0, errorValue,...
                        0, 0, 0);
                case {'DPSI','dpsi'}
                    obj.setMisalignment(madxElementName, ...
                        0, 0, 0,...
                        0, 0, errorValue);
                case {'DKR0', 'dkr0'}
                    obj.setFieldError(madxElementName, [], [], [errorValue], []);
                case {'DKR1', 'dkr1'}
                    obj.setFieldError(madxElementName, [], [], [0, errorValue], []);
                case {'DKR2', 'dkr2'}
                    obj.setFieldError(madxElementName, [], [], [0, 0, errorValue], []);
                case {'DK0', 'dk0'}
                    obj.setFieldError(madxElementName, [errorValue], [], [], []);
                case {'DK1', 'dk1'}
                    obj.setFieldError(madxElementName, [0, errorValue], [], [], []);
                case {'DK2', 'dk2'}
                    obj.setFieldError(madxElementName, [0, 0, errorValue], [], [], []);
                case {'DK3', 'dk3'}
                    obj.setFieldError(madxElementName, [0, 0, 0, errorValue], [], [], []);
                case {'DK4', 'dk4'}
                    obj.setFieldError(madxElementName, [0, 0, 0, 0, errorValue], [], [], []);
                case {'DK5', 'dk5'}
                    obj.setFieldError(madxElementName, [0, 0, 0, 0, 0, errorValue], [], [], []);
                case {'DKS0', 'dks0'}
                    obj.setFieldError(madxElementName, [], [errorValue], [], []);
                case {'DKS1', 'dks1'}
                    obj.setFieldError(madxElementName, [], [0, errorValue], [], []);
                case {'DKS2', 'dks2'}
                    obj.setFieldError(madxElementName, [], [0, 0, errorValue], [], []);
                case {'DKS3', 'dks3'}
                    obj.setFieldError(madxElementName, [], [0, 0, 0, errorValue], [], []);
                case {'DKS4', 'dks4'}
                    obj.setFieldError(madxElementName, [], [0, 0, 0, 0, errorValue], [], []);
                case {'DKS5', 'dks5'}
                    obj.setFieldError(madxElementName, [], [0, 0, 0, 0, 0, errorValue], [], []);
                otherwise
                    warning(['I don'' know this type of error! (',elementErrorType,')'])
            end
            
        end
        
        function RM = computeRMfromErrors(obj, madxElementNames, elementErrorTypes, probeErrorAmplitude,...
                observablesListOrIndexes, opticsFieldsNames)
            %RM = computeRMfromErrors(obj, madxElementNames, elementErrorTypes, probeErrorAmplitude,...
            %    observablesListOrIndexes, opticsFieldsNames)
            % it computes the response matrix due to misalignments or field errors of the
            % defined "madxElementNames". 
            % The type of error is inside the cellArray "elementErrorTypes"
            % "probeErrorAmplitude" is a single number and is supposed to be
            % the same for all type of errors. This is anyway normalised in
            % the calculation! but sometimes it might be important.
            % As for the conventional computeRM() function, the output is a
            % cell array of "cell(length(opticsFieldsNames),length(elementErrorTypes))"
            % with inside the different RM matrixes.
            % 
            % The computations are done for perfectly aligned/nominal machine.
            %
                
            
            % prepare output
            RM = cell(length(opticsFieldsNames),length(elementErrorTypes));
            
            % store initial settings %%%%%%%
            initialMisalignments = obj.misalignmentsMap;
            initialFieldErrors = obj.fieldErrorsMap;
            % remove all errors first
            obj.removeMisalignment();
            obj.removeFieldError();
            % compute plane optics
            initialOptic = obj.computeOptics();
            
            % do the real computation
            for i=1:length(madxElementNames)
                disp(['Element ',num2str(i),'/',num2str(length(madxElementNames))])
                %
                for j=1:length(elementErrorTypes)
                    %
                    obj.setGenericMachineImperfection(madxElementNames{i}, elementErrorTypes{j}, probeErrorAmplitude);
                    %
                    auxOptics = obj.computeOptics();
                    auxDelta = madx2matlab.computeDeltaOptics(auxOptics, initialOptic, observablesListOrIndexes, opticsFieldsNames);
                    for k=1:length(opticsFieldsNames)
                        RM{k,j}(:,i) = auxDelta(:,k)./probeErrorAmplitude;
                    end
                    obj.removeMisalignment();
                    obj.removeFieldError();
                end
            end
            
            % set back initial settings %%%%%%%
            obj.misalignmentsMap = initialMisalignments;
            obj.fieldErrorsMap = initialFieldErrors;
        end
        
        function RM = computeRM(obj, madxParametersListNames, X0, DeltaX, observablesListOrIndexes, opticsFieldsNames)
            % RM = computeRM(obj, madxParametersList, X0, DeltaX, observablesList, opticsFields)
            %
            % it computes the response matrix between parameters
            % "madxParametersList" and observables "observablesList" on the
            % optics fields "opticsFields".
            % one should also specify an initial value for the parameters
            % (X0), and the incremental value to use (DeltaX).
            %
            %
            % The output RM is a cell array with as many cells as the
            % number of opticsFields.
            
            % to be CASE INSENSITIVE
            if ~isnumeric(observablesListOrIndexes)
                observablesListOrIndexes = upper(observablesListOrIndexes);
            end
            madxParametersListNames = upper(madxParametersListNames);
            
            if length(madxParametersListNames) ~= length(X0) || length(X0) ~= length(DeltaX)
                error('Arguments "madxParametersList", "XO" and "DeltaX" must have the same length');
            end
            
            % save current model status:
            initialModelX = obj.getModelParameters(madxParametersListNames);
            
            % set initial conditions
            obj.setModelParameters(madxParametersListNames, X0);
            % compute intitial orbit
            initialOptic = obj.computeOptics();
            
            % prepare output
            RM = cell(length(opticsFieldsNames),1);
            
            % compute differences
            for i=1:length(madxParametersListNames)
                disp(['Parameter ',num2str(i),'/',num2str(length(madxParametersListNames))])

                % increase parameter
                obj.setModelParameters(madxParametersListNames{i},X0(i)+DeltaX(i));
                % compute Optics
                excitedOptic = obj.computeOptics();
                
                % calculate Linear Response
                auxDelta = madx2matlab.computeDeltaOptics(excitedOptic, initialOptic, observablesListOrIndexes, opticsFieldsNames);
                for j=1:length(opticsFieldsNames)
                    RM{j}(:,end+1) = auxDelta(:,j)/DeltaX(i);
                end
                
                % reset parameter
                obj.setModelParameters(madxParametersListNames{i},X0(i));
            end
            
            % reset model initial conditions
            obj.setModelParameters(madxParametersListNames, initialModelX);
        end
        
        function opticsTable = computeOptics(obj)
            % opticsTable = computeOptics(obj)
            % it computes the optics (twiss) TFS table and give it as output.
            
            % define my command
            myCommand = obj.twissCommandLine;
            myOutputCommandLines = obj.twissGetOutputCommandLine;
            
            % exec personal twiss script
            madResultLines = obj.generalExecMadxScript(myCommand, myOutputCommandLines);
            
            % parse Twiss table
            opticsTable = madx2matlab.parseTFSTable(madResultLines);
        end
        
        function apertureTable = computeAperture(obj)
            % apertureTable = computeAperture(obj)
            % since aperture need first a twiss, I will use first the
            % specified twissCommandLine, and then the specified
            % apertureCommandLine.
            
            % define my command
            if ~iscellstr(obj.twissCommandLine)
                aux1 = {obj.twissCommandLine};
            else
                aux1 = obj.twissCommandLine(:);
            end
            if ~iscellstr(obj.twissCommandLine)
                aux2 = {obj.apertureCommandLine};
            else
                aux2 = obj.apertureCommandLine(:);
            end
            myCommand = [aux1; aux2];
            myOutputCommandLines = obj.apertureGetOutputCommandLine;
            
            % exec personal twiss script
            madResultLines = obj.generalExecMadxScript(myCommand, myOutputCommandLines);
            
            % parse Twiss table
            apertureTable = madx2matlab.parseTFSTable(madResultLines);
        end
        
        
        function surveyTable = computeSurvey(obj)
            % surveyTable = computeSurvey(obj)
            % it computes the survey TFS table and give it as output.
            
            % define my command
            myCommand = obj.surveyCommandLine;
            myOutputCommandLines = obj.surveyGetOutputCommandLine;
            
            % exec personal survey script
            madResultLines = obj.generalExecMadxScript(myCommand, myOutputCommandLines);
            
            % extract TFSTable
            surveyTable = madx2matlab.parseTFSTable(madResultLines);
        end
        
        function trackTable = computeTracking(obj, timeOut)
            % trackTable = computeTracking(obj [, timeOut = 300])
            % it computes the track TFS table and give it as output.
            %
            % normally 5 minutes of timeout are reasonable, but if you have
            % a complex tracking, just specify a longer timeout (in sec)
            
            % setting a default timeout.
            if nargin == 1
                timeOut = 300;
            end
            
            % exec custom tracking script
            madResultLines = obj.generalExecMadxScript(obj.trackCommandLine, obj.trackGetOutputCommandLine, {}, timeOut);
            
            % extract TFSTable
            trackTable = madx2matlab.parseTFSTable(madResultLines);
        end
        
        function plotCurrentMachineSurvey(obj)
            % plotCurrentMachineSurvey(obj)
            % it computes the current optics and current survey. Out of
            % those two output TFS table it generates a nice plot of the
            % machine elements.
            
            twiss=obj.computeOptics();
            survey=obj.computeSurvey();
            madx2matlab.plotMachineSurvey(twiss, survey);
        end
        
        function madResultText = generalExecMadxScript(obj, customCommandLines, customGetOutputCommandLines, customFinalCommandLines, myTimeout)
            % madResultText = generalExecMadxScript(obj, customCommandLines [, customGetOutputCommandLines [, customFinalCommandLines [,myTimeout]]])
            % It executes some command lines and gives you the result just
            % as text. The command lines are executed after the madxScript
            % given during object creation, the afterScriptCommandLine, the
            % parameters assignments, the misalignments, and the field
            % errors.
            %
            % If customGetOutputCommandLines are given, then it will try
            % to give you the output only from these command lines.
            % Typically this is a "write, table=...;" statement for
            % example.
            % If this is not given, then the funtion will return you all
            % the generated output.
            %
            % You can also specify some customFinalCommandLines.
            % These will be executed after your customCommandLines and
            % customGetOutputCommandLines. These are expected not to give
            % interesting output, but might be needed for some purposes.
            %
            % If the program didn't finish after myTimeout (default 600
            % seconds), the programm will be closed.
            
            if nargin == 1
                customCommandLines = {};
                customGetOutputCommandLines = {};
                customFinalCommandLines = {};
                myTimeout = 600;
            elseif nargin == 2
                customGetOutputCommandLines = {};
                customFinalCommandLines = {};
                myTimeout = 600;
            elseif nargin == 3
                customFinalCommandLines = {};
                myTimeout = 600;
            elseif nargin == 4
                myTimeout = 600;
            end
            
            % convert to proper type
            if ~iscellstr(customCommandLines)
                customCommandLines = {customCommandLines};
            end
            if ~iscellstr(customGetOutputCommandLines)
                customGetOutputCommandLines = {customGetOutputCommandLines};
            end
            if ~iscellstr(customFinalCommandLines)
                customFinalCommandLines = {customFinalCommandLines};
            end
            
            % prepare script: %%%%%%%%%%%%%
            %%%
            % starting from the initialization scripts lines defined by
            % this object.
            scriptLines = obj.madxScriptLines(:);
            
            
            %%%
            % add parameter definitions
            allParametersName = obj.madxParametersMap.keys;
            auxNewLines = cell(length(allParametersName),1);
            for i=1:length(allParametersName)
                tmpParamStruct = obj.madxParametersMap(allParametersName{i});
                % increase precision!
                %auxNewLines{i} = [tmpParamStruct.name,' = ', num2str(tmpParamStruct.value),';'];
                auxNewLines{i} = sprintf('%s = %1.15d;', tmpParamStruct.name, tmpParamStruct.value);
            end
            scriptLines = [scriptLines; auxNewLines(:)];
            
            %%%
            % add misalignment definitions
            allMisalignmentPatterns = obj.misalignmentsMap.keys;
            auxNewLines = cell(length(allMisalignmentPatterns),1);
            for i=1:length(allMisalignmentPatterns)
                tmpMisalignmentStruct = obj.misalignmentsMap(allMisalignmentPatterns{i});
                auxNewLines{i} = [...
                    'SELECT,FLAG=ERROR,CLEAR;',char(10),...
                    'SELECT,FLAG=ERROR,PATTERN="',tmpMisalignmentStruct.pattern,'";',char(10),...
                    'EALIGN, DX = ',num2str(tmpMisalignmentStruct.dx,'%1.15d'),',',...
                    'DY = ',num2str(tmpMisalignmentStruct.dy,'%1.15d'),',',...
                    'DS = ',num2str(tmpMisalignmentStruct.ds,'%1.15d'),',',...
                    'DPHI = ',num2str(tmpMisalignmentStruct.dphi,'%1.15d'),',',...
                    'DTHETA = ',num2str(tmpMisalignmentStruct.dtheta,'%1.15d'),',',...
                    'DPSI = ',num2str(tmpMisalignmentStruct.dpsi,'%1.15d'),',',...
                    'MREX = ',num2str(tmpMisalignmentStruct.mrex,'%1.15d'),',',...
                    'MREY = ',num2str(tmpMisalignmentStruct.mrey,'%1.15d'),';'];
            end
            scriptLines = [scriptLines; auxNewLines(:)];
            
            %%%
            % add field errors definitions
            allFieldErrorsPatterns = obj.fieldErrorsMap.keys;
            auxNewLines = cell(length(allFieldErrorsPatterns),1);
            for i=1:length(allFieldErrorsPatterns)
                tmpFieldErrorsStruct = obj.fieldErrorsMap(allFieldErrorsPatterns{i});
                auxErrors = '';
                if ~isempty(tmpFieldErrorsStruct.dkn)
                    if ~isempty(tmpFieldErrorsStruct.dknr)
                        warning('Some relative errors are not used because of absolute errors defined');
                    end
                    auxErrors = [auxErrors, 'DKN = {',sprintf('%d, ',tmpFieldErrorsStruct.dkn),'},',char(10)];
                elseif ~isempty(tmpFieldErrorsStruct.dknr)
                    auxErrors = [auxErrors, 'DKNR = {',sprintf('%d, ',tmpFieldErrorsStruct.dknr),'},',char(10)];
                end
                if ~isempty(tmpFieldErrorsStruct.dks)
                    if ~isempty(tmpFieldErrorsStruct.dksr)
                        warning('Some relative skew errors are not used because of absolute errors defined');
                    end
                    auxErrors = [auxErrors, 'DKS = {',sprintf('%d, ',tmpFieldErrorsStruct.dks),'},',char(10)];
                elseif ~isempty(tmpFieldErrorsStruct.dksr)
                    auxErrors = [auxErrors, 'DKSR = {',sprintf('%d, ',tmpFieldErrorsStruct.dksr),'},',char(10)];
                end
                
                auxNewLines{i} = [ ...
                    'SELECT,FLAG=ERROR,CLEAR;',char(10),...
                    'SELECT,FLAG=ERROR,PATTERN="',tmpFieldErrorsStruct.pattern,'";',char(10),...
                    'EFCOMP,',...
                    auxErrors,' , RADIUS=1, ORDER=0;']; 
            end
            scriptLines = [scriptLines; auxNewLines(:)];
            
            %%%
            % add custom command lines function
            scriptLines = [scriptLines; customCommandLines(:)];
            
            %%%
            % add command lines that should give back an output.
            scriptLines = [scriptLines; ['set, format="',obj.madxFormat,'";']];
            customCommandLineBeforeStartIdx = length(scriptLines);
            scriptLines = [scriptLines; customGetOutputCommandLines(:)];
            customCommandLineEndIdx = length(scriptLines);
            
            %%%
            % add after-Command lines
            scriptLines = [scriptLines; customFinalCommandLines(:)];
            
            % debug
            if obj.debugMADX
                disp('I will run this script:');
                disp('-----------------------');
                for i=1:length(scriptLines)
                    disp(scriptLines{i});
                end
            end
            
            try
                % prepare system command
                auxRuntime = java.lang.Runtime.getRuntime();
                myProcess = auxRuntime.exec(obj.madxExecutable);
                myProcessInputStream = myProcess.getInputStream();
                myProcessOutputStream = myProcess.getOutputStream();
                myProcessErrorStream = myProcess.getErrorStream();
                
                auxBuffersSize = 2^24; % 2^26/1024/1024 = 64 MB!
                myProcessBufferedInput = java.io.BufferedReader( java.io.InputStreamReader( myProcessInputStream ), auxBuffersSize);
                myProcessBufferedOutput = java.io.BufferedWriter( java.io.OutputStreamWriter( myProcessOutputStream ), auxBuffersSize);
                myProcessBufferedErrorInput = java.io.BufferedReader( java.io.InputStreamReader( myProcessErrorStream ), auxBuffersSize);
                
                %%
                % start writing to MADX and keep reading...
                tic
                madResultText = cell(0);
                for i=1:customCommandLineBeforeStartIdx
                    myProcessBufferedOutput.write(scriptLines{i});
                    myProcessBufferedOutput.newLine();
                    myProcessBufferedOutput.newLine(); % adding a newline
                    myProcessBufferedOutput.flush();
                    
                    % keep reading if there is new data.
                    %%%%%%%%%%%%%%%%%%%%%%
                    % test 15/03/2018:
                    %   avoid reading if we know madx might
                    %   not have finished to write a complete line
                    %madResultText = [madResultText, madx2matlab.readProcessBufferedInput(myProcessBufferedInput)];
                    %%%%%%%%%%%%%%%%%%%%%%
                end
                myProcessBufferedOutput.flush();
                
                % cut before output marker
                if ~isempty(customGetOutputCommandLines)
                    myProcessBufferedOutput.write('print, text="thisismymarkerandihopeisuniquebegin";');
                    myProcessBufferedOutput.newLine();
                    myProcessBufferedOutput.flush();
                end
                
                % write the lines that will generate output.
                for i=(customCommandLineBeforeStartIdx+1):customCommandLineEndIdx
                    myProcessBufferedOutput.write(scriptLines{i});
                    myProcessBufferedOutput.newLine();
                end
                myProcessBufferedOutput.flush();
                
                % cut after output marker
                if ~isempty(customGetOutputCommandLines)
                    myProcessBufferedOutput.write('print, text="thisismymarkerandihopeisuniqueend";');
                    myProcessBufferedOutput.newLine();
                    myProcessBufferedOutput.flush();
                end
                
                
                % write final command lines
                for i=(customCommandLineEndIdx+1):length(scriptLines)
                    myProcessBufferedOutput.write(scriptLines{i});
                    myProcessBufferedOutput.newLine();
                end
                myProcessBufferedOutput.flush();
                
                % stop process (if not yet finished)
                myProcessBufferedOutput.write('stop;');
                myProcessBufferedOutput.newLine();
                myProcessBufferedOutput.flush();
                
                % read final output
                while true
                    madResultText = [madResultText, madx2matlab.readProcessBufferedInput(myProcessBufferedInput)];
                    try
                        myProcess.exitValue();
                        break;
                    catch e
                        % script still didnt' finished.
                        pause(0.1);
                        if toc > myTimeout
                            myProcess.destroy
                            error(['Timeout in executing madx script. Last read line: ',madResultText{end}])
                        end
                    end
                end
            catch e
                if obj.debugMADX
                    aux = madx2matlab.readProcessBufferedInput(myProcessBufferedInput);
                    disp(aux(:))
                end
                error(['Something wrong executing this script in madx... last error:', e.message]);
            end
            % check exit value
            if myProcess.exitValue ~= 0
                if obj.debugMADX
                    aux = madx2matlab.readProcessBufferedInput(myProcessBufferedErrorInput);
                    disp(aux(:))
                end
                auxNlines = length(madResultText);
                auxLastLines = '';
                for i=max(1,auxNlines-50):auxNlines
                    auxLastLines = [auxLastLines, char(10), madResultText{i}];
                end
                error(['Something wrong executing this script in madx... Last read lines: ',char(10), auxLastLines]);
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%
            % read what is given by madx, and cut only the interesting part
            % if a customGetOutputCommandLines was specified.
            if ~isempty(customGetOutputCommandLines)
                for i=1:length(madResultText)
                    if (length(madResultText{i}) >= 35) && ...
                            ~isempty(strfind(madResultText{i},'thisismymarkerandihopeisuniquebegin'))
                        madResultText = madResultText((i+1):end);
                        break;
                    end
                end
                for i=1:length(madResultText)
                    if ~isempty(strfind(madResultText{i},'thisismymarkerandihopeisuniqueend'))
                        madResultText = madResultText(1:(i-1));
                        break;
                    end
                end
            end
        end
    end
    
    methods(Static, Access=protected)
        function outputStrings = readProcessBufferedInput( processBufferedInput)
            outputStrings = cell(0);
            auxString = '';
            while (processBufferedInput.ready)
                % old way -> broken after MAD  5.02.07:
                outputStrings{end+1} = char(processBufferedInput.readLine);
                %disp(outputStrings{end})
                % WARNING!!!
                %  we will need now to read each single char and
                %  convert it to char and check when a newline
                %  arrives...
                %
%                 aux = processBufferedInput.read;
%                 %disp(aux);
%                 if aux == 10 % new line
%                     outputStrings{end+1} = auxString;
%                     disp(auxString)
%                     auxString = '';
%                 else
%                     auxString(end+1) = char(aux);
%                 end
                
            end
            %disp(['last: <',auxString,'>'])
        end
    end
    
    methods(Static)
        function lines=readFile(filename)
            % lines=readFile(filename)
            % simple static function to read a file and give you all the
            % lines in a structure.
            
            lines=cell(0);
            % open file
            fid=fopen(filename);
            
            % read the file
            i=0;
            while ~feof(fid)
                i=i+1;
                lines{i} = fgetl(fid);
            end
            disp([num2str(i), ' lines read from ', filename]);
            
            % close the file
            fclose(fid);
        end
        
        function lines=readData(data)
            %lines=readData(data)
            % it parses some text data line by line
            lines = textscan(data, '%s','delimiter',sprintf('\n'));% , 'BufSize', 100000);
            lines = lines{1};
        end
        
        function writeLinesToFile(lines, filename)
            % writeLinesToFile(lines, filename)
            % function provided for convenience. It writes to "filename"
            % the lines defined in the lines cell array. (similar structure
            % coming from a "readFile" function call... ;)
            
            if exist(fullfile(filename),'file') == 2
                reply = input(['File ',filename,' already exists.',char(10),...
                    'Do you want to continue? Y/N [N]:'],'s');
                if ~(~isempty(reply) && strcmpi(reply, 'Y'))
                    disp('... writing aborted by the user.')
                    return
                end
            end
            
            dcf=fopen(fullfile(filename),'w');
            
            for i=1:length(lines)
                fprintf(dcf,[lines{i},'\n']);
            end
            fclose(dcf);
        end
        
        function lines=parseScriptFile(filename)
            % lines=parseScriptFile(filename)
            % it reads a MADX script and return it as a "lines" structure.
            % It also removes all the comments and the empty lines.
            
            if isempty(filename)
                lines = {};
                return
            end
            
            lines = madx2matlab.readFile(filename);
            for i=1:length(lines)
                if ~isempty(regexpi(lines{i},'^( *!)'))
                    % remove comments
                    lines{i}='';
                elseif ~isempty(regexpi(lines{i},'^( *stop)'))
                    % break at the first stop entry
                    lines(i:end) = [];
                    break;
                end
            end
        end
        
        function parametersMap=parseParametersFile(filename)
            % parametersMap=parseParametersFile(filename)
            % it parses a file with madx parmaters (i.e. lines in the form
            % PARAMETER = value; ! comment
            % and extracts all these informations in a containers.Map();
            
            parametersMap=containers.Map();
            
            if isempty(filename)
                disp('No parameters file given, i.e. no need of parameters.')
                return;
            end
            
            fileLines = madx2matlab.readFile(filename);
            
            for i=1:length(fileLines)
                if ~isempty(regexpi(fileLines{i},'^( *!)'))
                    % skip comments
                    continue;
                end
                
                aux=regexp(fileLines{i},':=|=|!','split');
                if ~iscell(aux)
                    aux={aux};
                end
                
                if isempty(strtrim(aux{1}))
                    % empty line, continue
                    continue
                end
                
                auxLength=length(aux);
                
                tmpStruct = struct;
                % CASE INSENSITIVE: by convention, all the names are uppercase for me!
                tmpStruct.name = upper(strtrim(aux{1}));
                if auxLength > 1
                    try
                        % with str2num I can evaluate little operations
                        % with numbers.
                        %tmpStruct.value=strread(aux{2},'%f',1);
                        tmpStruct.value=str2num(aux{2});
                    catch e
                        disp(['Unable to read a value for parameter ',aux{1}, ' from parameters file.'])
                        tmpStruct.value=NaN;
                    end
                else
                    tmpStruct.value=NaN;
                end
                
                if auxLength > 2
                    tmpStruct.comment=strtrim([aux{3:end}]);
                else
                    tmpStruct.comment='';
                end
                
                parametersMap(tmpStruct.name) = tmpStruct;
            end
        end
        
        function writeParametersMapToFile(parametersMap, filename)
            % writeParametersMapToFile(parametersMap, filename)
            % Reverse of the function parseParametersFile(filename), it
            % writes a parametersMap (containers.Map() object) to the file
            % specified.
            
            % add parameter definitions
            allParametersName = parametersMap.keys;
            for i=1:length(allParametersName)
                tmpParamStruct = parametersMap(allParametersName{i});
                lines{i} = [tmpParamStruct.name,' = ', ...
                    num2str(tmpParamStruct.value,'%1.15d'),'; ', ...
                    '! ', tmpParamStruct.comment];
            end
            madx2matlab.writeLinesToFile(lines, filename);
        end
        
        function outputTables = parseTFSTableFromFile(filename)
            % outputTables = parseTFSTableFromFile(filename)
            % It parses the given file as a MAD-X TFS table.
            %
            % The returned structure contains all the informations
            % available in the table parsed according to the fields type
            % specified in the table itself.
            %
            
            mylines = madx2matlab.readFile(filename);
            outputTables = madx2matlab.parseTFSTable(mylines);
        end
        
        function outputTables = parseTFSTable(lines)
            % outputTables = parseTFSTable(lines)
            %
            % Thanks to Guido Sterbini. It parses some lines of text
            % getting out the TFS table common of any MADX output...
            % March 2015 -> parsing rewritten to allow also YASP tables
            % readings...
            %
            % The "lines" should be in a form of a cell array of strings
            % where each element correspond to a line of text relative to a
            % TFS table.
            %
            % The returned structure contains all the informations
            % available in the table parsed according to the fields type
            % specified in the table itself.
            %
            
            % save current settings for warnings.
            initialWarningStatus = warning;
            % temporary disable warnings
            %disp('DEBUG')
            %warning off all;
            
            % initialize output
            outputTables = {'delete me!'};
            
            % resetValues for current table
            mySingleDataTable = [];
            dataFieldNames = {};
            dataFieldTypes = {};
            auxTable = struct;
            dataTableSuffix = [];
            
            % a couple of other test variables.
            tableFound = false;
            tableContentFound = false;
            
            for j=1:numel(lines)
                readin = lines{j};
                
                % consider only valid lines
                %%% Unfrotunatelly this doesn't always work when loading a
                %%% generic TFS table from file.
                % if isempty(readin) || isempty(regexp(readin, '^(\@| *"| *(0-9)|\*|\$)', 'once'));
                %      continue
                %  end
                
                % discard empty lines
                if isempty(readin)
                    continue
                end
                
                % we are looking for a table, so we need to see first a
                % line starting with ^@
                auxTest = strcmp(readin(1),'@'); %~isempty(regexp(readin,'^@', 'once'));
                
                if (~tableFound) && ~auxTest
                    continue
                elseif tableContentFound && auxTest
                    % this means we are in a new table respect to an older
                    % one! getting a new table.
                    
                    % store the old one (after storing the current
                    % dataTable)
                    auxTable.(['DATA',num2str(dataTableSuffix)]) = mySingleDataTable;
                    outputTables{end+1} = auxTable;
                    
                    % re-initialize variables
                    mySingleDataTable = [];
                    dataFieldNames = {};
                    dataFieldTypes = {};
                    auxTable = struct;
                    dataTableSuffix = [];
                    
                    tableFound = true;
                    tableContentFound = false;
                else
                    % this is the first table found. We can look for
                    % content.
                    tableFound = true;
                end
                
                
                %%%% this is very time consuming! %%%
                %TTT
                %auxParts = regexp(strtrim(readin),'( *|\t*)','split');
                %%%%                              %%%%
                %
                % March 2019: maybe possible to load tables that do not start with space.... 
                
                    
                if strcmp(readin(1),'@') %strcmp(auxParts{1}(1),'@') %regexp(auxParts{1},'^@')
                    % we are in the summary table
                    
                    %TT added
                    auxParts = regexp(strtrim(readin),'( *|\t*)','split');
                    
                    auxFieldName = strrep(auxParts{2},'-','_');
                    auxType = auxParts{3};
                    
                    if regexp(auxType,'s$')
                        auxParts{4} = strrep(auxParts{4},'"','');
                    end
                    auxValue = sscanf(auxParts{4},auxType);
                    auxTable.(auxFieldName) = auxValue;
                    
                elseif strcmp(readin(1),'*') %regexp(auxParts{1},'^\*')
                    % These are the names of the fields in the following
                    % table
                    
                    %TT added
                    auxParts = regexp(strtrim(readin),'( *|\t*)','split');
                    
                    % let's see if we already had a table
                    if ~isempty(dataFieldNames)
                        % store old table
                        auxTable.(['DATA',num2str(dataTableSuffix)]) = mySingleDataTable;
                        % reset current table
                        mySingleDataTable = [];
                        % increase prefix
                        if isempty(dataTableSuffix)
                            dataTableSuffix = 1;
                        else
                            dataTableSuffix = dataTableSuffix+1;
                        end
                    end
                    
                    % new data fields names
                    dataFieldNames = strrep(auxParts(2:end),'-','_');
                    
                    %TT del
                    % generate default types
                    %dataFieldTypes = cell(length(dataFieldNames),1);
                    %for i=1:length(dataFieldNames)
                    %    dataFieldTypes{i} = '%s';
                    %end
                    %TT add
                    dataFieldTypes = repmat('%q ',1,length(dataFieldNames));
                    
                     % remove empty
                    dataFieldNames = dataFieldNames(~cellfun('isempty',dataFieldNames));
                    
                   
                    
                    
                    % save fields and types to final table
                    auxTable.(['FIELDS',num2str(dataTableSuffix)]) = dataFieldNames;
                    auxTable.(['FIELDSTYPES',num2str(dataTableSuffix)]) = dataFieldTypes;
                    
                elseif strcmp(readin(1),'$') % regexp(auxParts{1},'^\$')
                    % These are the data type of the following table
                    
                    %TT del
                    %dataFieldTypes = auxParts(2:end);
                    %TT added
                    
                    dataFieldTypes = strtrim(readin(2:end));
                    dataFieldTypes = regexprep(dataFieldTypes, '%[0-9]*s', '%q');
                    dataFieldTypes = regexprep(dataFieldTypes, '%hd', '%d8');
                    dataFieldTypes = regexprep(dataFieldTypes, '%le', '%f');
                    
                    auxTable.(['FIELDSTYPES',num2str(dataTableSuffix)]) = dataFieldTypes;
                    
                elseif strcmp(readin(1),'#') % regexp(auxParts{1},'^\#')
                    % This is some other unknow field... give a warning
                    warning(['I found a line starting with #: ', readin, '. But I don''t know what it is...'])
                    
                else
                    % This is some other unknow field... give a warning
                    % warning(['I found this line: ', readin, '. And I treat is as data'])
                
                    % if strcmp(readin(1),' ') || strcmp(readin(1),'\t') || strcmp(readin(1),'"') || strcmp(readin(1),'[0-9]') % regexp(readin(1:2),'^( |\t)')
                    % we are in the DATA part. adding a line to the current data
                    % table.
                    
                    % remember it.. ;)
                    tableContentFound = true;
                    
                    %TT added
                    auxDataLine = textscan(readin, dataFieldTypes);
                    if length(auxDataLine) ~= length(dataFieldNames)
                        error('Something wrong with some data line... (less elements than expected).')
                    end
                    
                    aux = struct;
                    for i=1:length(dataFieldNames)
                        %TT del
                        %aux.(dataFieldNames{i}) = sscanf(strrep(auxParts{i}, '"',''),dataFieldTypes{i});
                        %TT added
                        if iscell(auxDataLine{i})
                            aux.(dataFieldNames{i}) = auxDataLine{i}{:};
                        else
                            aux.(dataFieldNames{i}) = auxDataLine{i};
                        end
                    end
                    
                    if isempty(mySingleDataTable)
                        mySingleDataTable = aux;
                    else
                        mySingleDataTable(end+1) = aux;
                    end
                   
                end
            end % for all lines 
            % store last data table in the current table, and add it to the
            % output.
            auxTable.(['DATA',num2str(dataTableSuffix)]) = mySingleDataTable;
            outputTables{end+1} = auxTable;
            
            % the first element was fake!
            if length(outputTables) > 2
                outputTables = outputTables(2:end);
            elseif length(outputTables) == 2
                outputTables = outputTables{2};
            else
                outputTables = [];
            end
                
            
        end % function
        
        function delta = computeDeltaOptics(excitedOptic, zeroOptic, observablesListorIdxes, opticsFields)
            % delta = computeDeltaOptics(excitedOptic, zeroOptic, observablesList, opticsFields)
            % Given an excited optics and a reference optics, it computes
            % the difference of optics fields values specified for the
            % wanted observablesList.
            %
            %
            % If you ask for more than one optics field, the output will be
            % a matrix with as many columns as the number of optics
            % fields.
            
            excitedValues = madx2matlab.extractOpticFieldsValues(excitedOptic, observablesListorIdxes, opticsFields);
            zeroValues = madx2matlab.extractOpticFieldsValues(zeroOptic, observablesListorIdxes, opticsFields);
            delta = excitedValues - zeroValues;
        end
        
        function varargout = extractOpticFieldsValues(inputOptic, observablesListorIdxes, opticsFields)
            % varargout = extractOpticFieldsValues(inputOptic, observablesList, opticsFields)
            %
            % It extract form "inputOptic" the value of elements
            % "observablesListorIdxes" of their "opticsFields".
            % "observablesListorIdxes" can also simply be an array of
            % indexes in the optic structure.
            %
            % If nargout == 1, only the values found are given.
            % If nargout == 2, the second output are the indexes of the
            % inputOptic where the values has been found.
            
            if ~iscell(observablesListorIdxes) && ischar(observablesListorIdxes)
                observablesListorIdxes = {observablesListorIdxes};
            end   
            %
            if isnumeric(observablesListorIdxes)
                foundObservablesIndex = observablesListorIdxes;
            else
                % look for the indexes of the interesting observables
                foundObservablesIndex = madx2matlab.findElement(inputOptic, 'NAME', observablesListorIdxes);
            end
            
            %
            if ~iscell(opticsFields)
                opticsFields = {opticsFields};
            end
            
            % fill output
            nFields = length(opticsFields);
            nObservables = length(foundObservablesIndex);
            output = NaN(nObservables,nFields);
            
            notNaNFoundObservablesIndex = ~isnan(foundObservablesIndex);
            for i = 1:nFields;
                try
                    values = [inputOptic.DATA.(opticsFields{i})];
                    values = values(:);
                    %
                    output(notNaNFoundObservablesIndex,i) = values(foundObservablesIndex(notNaNFoundObservablesIndex));
                catch exception
                    error(['Unable to extract OpticField "',opticsFields{i},'": ', exception.message]);
                end
            end
            
            % prepare final output
            varargout{1} = output;
            if nargout == 2
                varargout{2} = foundObservablesIndex;
            end
        end
        
        function output_axes = plotMachineTwissFunctions(twissTable, plotLayout, madLikeLayout, sOffset)
            %output_axes = plotMachineTwissFunctions(twissTable, plotLayout, madLikeLayout, sOffset)
            % It generates a plot with betas and h dispersion for the given
            % twiss table.
            % if plotLayout is true (default = true), then the lattice layout is also
            % represented.
            % if madLikeLayout is true (default = false), then the lattice
            % layout is generated using a more standard symbols. 
            % if sOffset is specified, then this is REMOVED from the longitudinal
            % coordinate present in the twissTable "DATA.S" field.
            %
            
            if nargin == 1
                plotLayout = true;
                madLikeLayout = false;
                sOffset = 0;
            elseif nargin == 2
                madLikeLayout = false;
                sOffset = 0;
            elseif nargin == 3
                sOffset = 0;
            end
            
            % extract functions
            auxS = [twissTable.DATA.S] - sOffset;
            if isfield(twissTable, 'TYPE')
                auxName = twissTable.TYPE;
            elseif isfield(twissTable, 'NAME')
                auxName = twissTable.NAME;
            else % assume is normal twiss
                auxName = 'unknownTableType';
            end
            if strcmpi(auxName, 'ptc_twiss')
                auxBetaX = [twissTable.DATA.BETA11];
                auxBetaY = [twissTable.DATA.BETA22];
                auxDispX = [twissTable.DATA.DISP1];
            else
                auxBetaX = [twissTable.DATA.BETX];
                auxBetaY = [twissTable.DATA.BETY];
                auxDispX = [twissTable.DATA.DX];
            end
            
            if plotLayout
                % clear current figure
                clf
                output_axes = [];
                %subplot('Position',positionVector) positionVector := [left,bottom,width,height]
                output_axes(1) = subplot('Position',[0.1,0.1,0.85,0.1]);
                
                % plot the machine elements
                madx2matlab.plotMachineTwissElements(twissTable, 0.8, madLikeLayout, sOffset);
                ylim([-1 1])
                set(output_axes(1), 'YTick', [], 'Box', 'on', 'FontSize',16)
                xlabel('s [m]','FontSize',16)
                grid on
                
                % prepare axes for twiss functions
                output_axes(2) = subplot('Position',[0.1,0.2,0.85,0.75]);
            else
                output_axes = gca;
            end
            
            % plot the twiss functions
            plot(auxS, auxBetaX,'b','LineWidth',2)
            hold on
            plot(auxS, auxBetaY,'r','LineWidth',2)
            plot(auxS, 10*auxDispX,'Color',[0 0.5 0],'LineWidth',2)
            hold off
            grid on
            legend('\beta_x','\beta_y', '10 D_x')
            ylabel('[m]','FontSize',16)
            if ~plotLayout
                xlabel('s [m]','FontSize',16)
                xlim([min(auxS)-0.1, max(auxS)+0.1])
                set(gca,'FontSize',16)
            else
                % link axes
                linkaxes(output_axes,'x')
                % set some paramaters
                set(output_axes(2), 'XTickLabel', [], 'Box', 'on' ,'FontSize',16)
                
                % fix xlim
                xlim(output_axes(2), [min(auxS)-0.1, max(auxS)+0.1])
            end
        end
        
        function plotMachineSurvey(twissTable, surveyTable)
            %plotMachineSurvey(twissTable, surveyTable)
            %
            
            % SBENDs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','SBEND');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.8,.8,'b',.4,4);
            
            % RBENDs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','RBEND');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.8,.6,'b',.4,4);
            
            % HKICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','HKICKER');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.4,.4,'y',.6,4);
            
            % VKICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','VKICKER');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.4,.4,'y',.6,4);
            
            % KICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','KICKER');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.4,.4,'r',.6,4);
            
            % QUADRUPOLEs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','QUADRUPOLE');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.8,.8,'r',.4,8);
            
            % SEXTUPOLEs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','SEXTUPOLE');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.4,.4,'r',.6,12);
            
            % MONITORs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','MONITOR');
            madx2matlab.plotSurveySingleElements(myIndex,surveyTable,.2,.2,'g',.8,24);
            
            % set a standard view
            axis off equal
            view(180,0)
        end
        
        function plotMachineTwissElements(twissTable, yscaleAndOffset, madLikeLayout, sOffset, displayNames)
            % plotMachineTwissElements(twissTable [, yscaleAndOffset = 1, madLikeLayout = false, sOffset = 0, displayNames])
            % static method to plot the machine elements included in the
            % given twissTable.
            % yscaleAndOffset (default = [1,0]) is the Y amplitude of the elements and an optional y offset.
            %  It can be convinient when overlapping the layout to some other
            %  plot.
            % if madLikeLayout = true (default false) it will use a more
            %  "standard" layout style.
            % sOffset is a single number, used to offset the longitudinal
            %  positions of the elements (i.e. to recentre around some
            %  specific element)
            % displayNames is if ~= 0 it plots the names of the devices.
            %  The value you specify can be used for adjusting the
            %  transverse offset of the name with respect to the object
            %
            % NOTE: the twissTable has to include the column KEYWORD, L, S
            % and K1L
            %
            
            if nargin == 1
                yscaleAndOffset = 1;
                madLikeLayout = false;
                sOffset = 0;
                displayNames = 0;
            elseif nargin == 2
                madLikeLayout = false;
                sOffset = 0;
                displayNames = 0;
            elseif nargin == 3
                sOffset = 0;
                displayNames = 0;
            elseif nargin == 4
                displayNames = 0;
            end
            
            % some baseline
            minS = min([twissTable.DATA.S]-sOffset);
            maxS = max([twissTable.DATA.S]-sOffset);
            if length(yscaleAndOffset) > 1
                line([minS maxS], yscaleAndOffset(2).*[1 1],'Color','k');
            else
                line([minS maxS], [0 0],'Color','k');
            end
            
            % SBENDs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','SBEND');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'bend', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % RBENDs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','RBEND');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'bend', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % HKICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','HKICKER');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'kicker', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % VKICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','VKICKER');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'kicker', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % KICKERs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','KICKER');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'kicker', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % QUADRUPOLEs
            myIndex = [...
                madx2matlab.findElement(twissTable,'KEYWORD','QUADRUPOLE'),...
                madx2matlab.findElement(twissTable,'KEYWORD','MULTIPOLE')];
            myK1L = [twissTable.DATA(myIndex).K1L];
            myFocusingIdxes = myIndex(myK1L > 0);
            myDeFocusingIdxes = myIndex(myK1L < 0);
            madx2matlab.plotTwissSingleElements(myFocusingIdxes, twissTable, 'focusing', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            madx2matlab.plotTwissSingleElements(myDeFocusingIdxes, twissTable, 'defocusing', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % SEXTUPOLEs
            myIndex = [...
                madx2matlab.findElement(twissTable,'KEYWORD','SEXTUPOLE'),...
                madx2matlab.findElement(twissTable,'KEYWORD','MULTIPOLE')];
            try
                myK2L = abs([twissTable.DATA(myIndex).K2L]);
            catch e
                warning('I canot get K2L from give TWISS table...')
                myK2L = zeros(length(twissTable.DATA),1);
            end
            myIndex = myIndex(myK2L > 0);
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'sextupole', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % COLLIMATORs
            myIndex = madx2matlab.findElement(twissTable,'KEYWORD','.?COLLIMATOR');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'collimator', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
            
            % MONITORs
            myIndex=madx2matlab.findElement(twissTable,'KEYWORD','MONITOR');
            madx2matlab.plotTwissSingleElements(myIndex, twissTable, 'monitor', yscaleAndOffset, madLikeLayout, sOffset, displayNames);
        end
        
        function plotTwissSingleElements(listIdxs, twiss, elementsType, yscaleAndOffset, madLikeLayout, sOffset, showNames)
            if nargin == 3
                yscaleAndOffset = 1;
                madLikeLayout = false;
                sOffset = 0;
                showNames = 0;
            elseif nargin == 4
                madLikeLayout = false;
                sOffset = 0;
                showNames = 0;
            elseif nargin == 5
                sOffset = 0;
                showNames = 0;
            elseif nargin == 6
                showNames = 0;
            end
            %
            % coordinates to be used to create patches
            if madLikeLayout
                bendingXCoordinates = [0 0 1 1];
                bendingYCoordinates = [-0.7 0.7 0.7 -0.7];
                %
                kickerXCoordinates = [0 0 1 1];
                kickerYCoordinates = [-1 1 1 -1]*0.5;
                %
                focusingXCoordinates = [0 0 1 1];
                focusingYCoordinates = [0 1 1 0];
                %
                defocusingXCoordinates = [0 0 1 1];
                defocusingYCoordinates = [-1 0 0 -1];
                %
                sextupoleXCoordinates  = [0 0.5 1 0.5];
                sextupoleYCoordinates  = [0 1 0 -1];
                %
                collimatorXCoordinates = [0   0 1   1    0  0  1    1   0];
                collimatorYCoordinates = [0.5 1 1 0.5 -0.5 -1 -1 -0.5 0.5];
                %
                monitorXCoordinates  = [0.5 0.5];
                monitorYCoordinates  = [-1 1];
            else
                bendingXCoordinates = [0 0 1 1];
                bendingYCoordinates = [-1 1 1 -1];
                %
                kickerXCoordinates = [0 1 0 1];
                kickerYCoordinates = [1 -1 -1 1]*0.5;
                %
                focusingXCoordinates = [0 0.2 0.5 0.8 1  0.8 0.5  0.2];
                focusingYCoordinates = [0 0.7   1 0.7 0 -0.7  -1 -0.7];
                %
                defocusingXCoordinates = [0 0.3 0.5  0.3  0  1  0.7 0.5 0.7 1];
                defocusingYCoordinates = [1 0.7   0 -0.7 -1 -1 -0.7   0 0.7 1];
                %
                sextupoleXCoordinates  = [0 0.5 1 0.5];
                sextupoleYCoordinates  = [0 1 0 -1];
                %
                collimatorXCoordinates = [0   0 1   1    0  0  1    1   0];
                collimatorYCoordinates = [0.5 1 1 0.5 -0.5 -1 -1 -0.5 0.5];
                %
                monitorXCoordinates  = [0.5 0.5];
                monitorYCoordinates  = [-1 1];
            end
            
            % 
            if length(yscaleAndOffset) > 1
                yscale = yscaleAndOffset(1);
                yoffset = yscaleAndOffset(2);
            else
                yscale = yscaleAndOffset(1);
                yoffset = 0;
            end
            
            % plot
            for i=1:length(listIdxs)
                tmpLength = twiss.DATA(listIdxs(i)).L;
                tmpName = twiss.DATA(listIdxs(i)).NAME;
                tmpInitialPosition  = twiss.DATA(listIdxs(i)).S - tmpLength - sOffset;
                % I force to have at least 10 cm long elements
                if tmpLength < 0.1
                    tmpInitialPosition = tmpInitialPosition + tmpLength/2 - 0.1/2;
                    tmpLength = 0.1;
                end
                tmpLength = max(tmpLength,0.1);
                %
                tmpCentre = tmpInitialPosition + tmpLength/2;
                
                switch elementsType
                    case 'bend'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'r';
                        tmpXdata = bendingXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = bendingYCoordinates*yscale + yoffset;
                    case 'kicker'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'g';
                        tmpXdata = kickerXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = kickerYCoordinates*yscale + yoffset;
                    case 'focusing'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'b';
                        tmpXdata = focusingXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = focusingYCoordinates*yscale + yoffset;
                    case 'defocusing'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'b';
                        tmpXdata = defocusingXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = defocusingYCoordinates*yscale + yoffset;
                    case 'sextupole'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'y';
                        tmpXdata = sextupoleXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = sextupoleYCoordinates*yscale + yoffset;
                    case 'collimator'
                        tmpLineStyle = '-';
                        tmpFaceColor = 'y';
                        tmpXdata = collimatorXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = collimatorYCoordinates*yscale + yoffset;    
                    case 'monitor'
                        tmpLineStyle = '--';
                        tmpFaceColor = 'none';
                        tmpXdata = monitorXCoordinates*tmpLength + tmpInitialPosition;
                        tmpYdata = monitorYCoordinates*yscale + yoffset;
                    otherwise
                        error('madx2matlab::plotTwissSingleElements: Unknown element Type');
                end
                
                % do the real plot
                patch(tmpXdata, tmpYdata,'k','FaceColor',tmpFaceColor,'LineStyle',tmpLineStyle)
                
                % add name
                if showNames ~= 0
                    if showNames > 0
                        auxalign = 'left';
                    else
                        auxalign = 'right';
                    end
                    text(tmpCentre, showNames*yscale*1.1 + yoffset, tmpName, 'Rotation',90,...
                        'FontWeight','bold', 'HorizontalAlignment',auxalign)
                end
            end
        end
        
        function output_args = plotSurveySingleElements(list, survey, dimx, dimy, color, alpha, faces)
            output_args = struct;
            for i=1:length(list)
                output_args(i).handle=patchObject(faces);
                output_args(i).handle.offset=[survey.DATA(list(i)).X  0 survey.DATA(list(i)).Z];
                output_args(i).handle.rotationAngles=[0  (180/pi)*(survey.DATA(list(i)).THETA)+(180/pi)*(survey.DATA(list(i)).ANGLE)/2 0 ];
                
                output_args(i).handle.color=color;
                output_args(i).handle.scale=[dimx dimy survey.DATA(list(i)).L ];
                
                output_args(i).handle.name=survey.DATA(list(i)).NAME;
                
                output_args(i).handle.drawIt;
                set(output_args(i).handle.handle,'FaceAlpha',alpha);
            end
        end
        
        function foundElementIndexes = findElement(TFSTableStructure, fieldNameInTFSTable, elementNameRegexp)
            %FINDELEMENT findElement( structure, fieldNameInTFSTable, elementNameRegexp )
            %   It returns the index of a MADX TFS table DATA structure
            %   that at which the interesting elementNameRegexp are found
            %   to be.
            %   If elementNameRegexp is single string, that all the
            %   occurance of such a string are reported.   
            %      -> i.e. easiest search
            %   If elementNameRegexp is a cell array of strings, then each
            %   string is interpreted and the system will try to give you
            %   either the first occurance or the desired occurance of a
            %   particular element.
            %      -> much more complex search.
            %      -> it returns an array of indexes that is ALWAYS as long
            %      as the cell array of names provided.
            
            
            if ischar(elementNameRegexp)
                allObservablesNames = {TFSTableStructure.DATA.(fieldNameInTFSTable)};
                aux = ~cellfun(@isempty, regexpi(allObservablesNames, elementNameRegexp));
                foundElementIndexes = find(aux);
                
            elseif iscell(elementNameRegexp)
                allObservablesNames = {TFSTableStructure.DATA.(fieldNameInTFSTable)};
                nAllObservables =  length(allObservablesNames);
                nObservables = length(elementNameRegexp);
                % here is the index of where the requested elements are.
                foundElementIndexes = nan(nObservables,1);
                %
                for j=1:nObservables
                    auxObs = elementNameRegexp{j};
                    auxObsIdxToSearch = 1;
                    auxObsNFound = 0;
                    aux = regexpi(auxObs,'\[([0-9]*?)\]$','match');
                    if ~isempty(aux) && length(aux{1}) > length(auxObs)
                        % just take the relevant part of the name
                        auxObs = auxObs(1:end-length(aux{1}));
                        try
                            auxObsIdxToSearch = sscanf(aux{1},'[%d]');
                        catch e
                            warning(['Didn''t understand the meaning of ', aux{1}, ' in ', auxObs,'.', char(10), 'Will give you first element']);
                            auxObsIdxToSearch = 1;
                        end
                    end
                    
                    for i=1:nAllObservables
                        auxCurName = strrep(allObservablesNames{i}, char(39),'');
                        auxCurName = strrep(auxCurName, '"','');
                        if (strcmpi(auxCurName, auxObs))
                            auxObsNFound = auxObsNFound+1;
                            if auxObsNFound == auxObsIdxToSearch
                                foundElementIndexes(j) = i;
                                break
                            end
                        end
                    end
                end
            end
        end
        
        function cellOutput = parseCSVFile(fileName, removeSpeechMarks)
            % cellOutput = parseCSVFile(fileName, removeSpeechMarks)
            % It reads fileName that is supposed to be a table in csv format.
            % The output cell, is matrix rapreseting all the elemnts of the
            % table as string.
            if nargin < 2
                removeSpeechMarks = false;
            end
            % read file
            fileLines = madx2matlab.readFile(fileName);
            
            nLines = numel(fileLines);
            if nLines < 1
                error('it seems that the file provided has no lines... check it!');
            end
            
            % read all lines
            cellOutput = cell(nLines,0);
            for i=1:nLines
                auxLineElements = regexp(fileLines{i},',','split');
                auxNElementsLine = numel(auxLineElements);
                
                for j=1:auxNElementsLine
                    cellOutput(i,j) = auxLineElements(j);
                end
            end
            
            if removeSpeechMarks
                cellOutput = strrep(cellOutput,'"','');
            end
        end
    end
end

