classdef patchObject < handle
    %PATCHOBJECT Summary of this class goes here
    %   Detailed explanation goes here
    %
    % Sterbini Feb 2013
    
    properties
        vertices
        faces
        color=[1 0 0]
        offset=[0 0 0]
        rotationAngles=[0 0 0]
        scale=[1 1 1];
        handle
        name='set MADX name'
    end
    
    properties (SetAccess=protected)
        rotatedVertices
        movedVertices
        scaledVertices
    end
    
    methods
        
        function obj=patchObject(n)
            angle=linspace(0,2*pi,n+1);
            angle=angle(1:(end-1))+pi/n;
            a=patch(cos(angle)/2,sin(angle)/2,'r','FaceAlpha',1,'Visible','off');
            aux=get(a,'Vertices');
            
            if size(aux,1)==3
                aux1=[aux;aux];
                aux1=[aux1 [zeros(size(aux,1),1); -ones(size(aux,1),1)]];
                myFaces=get(a,'Faces');
                nMyFaces=length(myFaces);
                myFaces=[[myFaces NaN];[myFaces NaN]+nMyFaces];
                for i=1:(nMyFaces-1)
                    myFaces=[myFaces; [myFaces(1,i:(i+1)) myFaces(2,(i+1):-1:i)]];
                end
                myFaces=[myFaces; [myFaces(1,[nMyFaces 1]) myFaces(2,[ 1 nMyFaces])]];
            else
                aux1=[aux;aux];
                aux1=[aux1 [zeros(size(aux,1),1); -ones(size(aux,1),1)]];
                myFaces=get(a,'Faces');
                nMyFaces=length(myFaces);
                myFaces=[[myFaces ];[myFaces ]+nMyFaces];
                for i=1:(nMyFaces-1)
                    myFaces=[myFaces; [myFaces(1,i:(i+1)) myFaces(2,(i+1):-1:i) ones(1,size(aux,1)-4)*NaN]];
                end
                myFaces=[myFaces; [myFaces(1,[nMyFaces 1]) myFaces(2,[ 1 nMyFaces]) ones(1,size(aux,1)-4)*NaN]];
                
            end
            
            obj.vertices=aux1;
            obj.faces=myFaces;
            delete(a)
            
        end
        
        function drawIt(obj)
            scaleIt(obj)
            rotateIt(obj,obj.rotationAngles)
            moveIt(obj,obj.offset)
            obj.handle=patch('Faces',obj.faces,'Vertices',obj.movedVertices,'facecolor',obj.color,'ButtonDownFcn',@(h,e) obj.myButtonDownFcn(h,e));
        end
        function moveIt(obj,offset)
            obj.movedVertices= obj.rotatedVertices+repmat(obj.offset,size(obj.vertices,1),1);
        end
        function rotateIt(obj,rotationAngles)
            aux=patch('Faces',obj.faces,'Vertices',obj.scaledVertices,'Visible','off');
            rotate(aux, [1 0 0],rotationAngles(1),[0 0 0]);
            rotate(aux, [0 1 0],rotationAngles(2),[0 0 0]);
            rotate(aux, [0 0 1],rotationAngles(3),[0 0 0]);
            obj.rotatedVertices= get(aux, 'Vertices');
            delete(aux);
        end
        function scaleIt(obj)
            obj.scaledVertices= obj.vertices.*repmat(obj.scale,size(obj.vertices,1),1);
        end
        function myButtonDownFcn(obj,h,e)
            disp(obj.name)
        end
    end
    
end

