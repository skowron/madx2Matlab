function [ alfa, beta, emittance ] = simpleFitBeam( x, angles )
%FITBEAM Summary of this function goes here
%   Detailed explanation goes here

% auxMat=zeros(2);
% auxMat(1,1) = mean(x.^2);
% auxMat(2,2) = mean(angles.^2);
% auxMat(1,2) = mean(x.*angles);
% auxMat(2,1) = mean(x.*angles);
auxMat = cov(x,angles);

emittance = sqrt(det(auxMat));

% beta = power(std(x),2)/emittance;
beta = auxMat(1,1)/emittance;
alfa = -auxMat(1,2)/emittance;

end

