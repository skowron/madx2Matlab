function [Xs, Angles] = generateParticles(nparticles, alpha, beta, stdgeoemittance)
% [Xs, Angles] = generateParticles(nparticles, alpha, beta, stdgeoemittance)
% generate nparticles accordingly to given Twiss parameters
%
myGamma = (1+alpha^2)/beta;
mySigma = [beta*stdgeoemittance -alpha*stdgeoemittance;...
    -alpha*stdgeoemittance myGamma*stdgeoemittance];

auxMat = mvnrnd([0 0], mySigma, nparticles);
Xs = auxMat(:,1);
Angles = auxMat(:,2);

end
